<?php

namespace yuankezhan\htmlHelper\options;

class SelectOptions extends Options
{
    /**
     * @var KeyValuePair[] $items 下拉菜单的选项
     */
    public $items;

    public function toArrayExcept()
    {
        return array_merge(parent::toArrayExcept(), ['items']);
    }

}