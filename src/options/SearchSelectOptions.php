<?php

namespace yuankezhan\htmlHelper\options;


class SearchSelectOptions extends Options
{
    public $selectOnly = false;//仅选择模式，不允许输入查询关键字
    public $pageSize = 15;
    public $multiple = false;//启用多选模式
    public $keyField = "key";
    public $showField = "value";
    public $requestUrl = "";
    public $data = [];
}