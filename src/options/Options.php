<?php

namespace yuankezhan\htmlHelper\options;


class Options extends BaseObject
{
    public $value;
    public $name;
    public $id;
    public $class;
    public $placeholder;
    public $title;
    public $style;
    public $type;
    public $onclick;
    public $readonly;
    public $disabled;
    public $onchange;
    public $onblur;


    /**
     * @var string $content 内容
     */
    public $content = "";

    public function toArrayExcept()
    {
        return ['content'];
    }
}