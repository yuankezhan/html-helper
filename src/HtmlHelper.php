<?php
namespace yuankezhan\htmlHelper;
use yuankezhan\htmlHelper\html\SearchSelect;
use yuankezhan\htmlHelper\options\KeyValuePair;
use yuankezhan\htmlHelper\html\Button;
use yuankezhan\htmlHelper\html\DateTime;
use yuankezhan\htmlHelper\html\Input;
use yuankezhan\htmlHelper\html\Select;
use yuankezhan\htmlHelper\html\Tag;

class HtmlHelper
{
    public static function input($options = [])
    {
        return (new Input($options))->create();
    }

    public static function hiddenInput($options = [])
    {
        return (new Input(array_merge(['type' => 'hidden'], $options)))->create();
    }

    public static function password($options = [])
    {
        return (new Input(array_merge(['type' => 'password'], $options)))->create();
    }

    /**
     * @param KeyValuePair[] $items
     * @param $options
     * @return mixed|string
     *
     * HtmlHelper::select([
     *   KeyValuePair::set(['key' => "1", 'value' => "公司1"]),
     *   KeyValuePair::set(['key' => "2", 'value' => "公司2"]),
     * ], ['style' => "fontsize:16px"])
     *
     */
    public static function select($options = [])
    {
        return (new Select($options))->create();
    }

    /**
     * 可搜索的select
     * @return void
     */
    public static function searchSelect($options = [])
    {
        if (empty($options["id"]))
        {
            $options["id"] = "s" . uniqid();
        }
        return (new SearchSelect($options))->create();
    }

    /**
     * 日期选择控件
     */
    public static function dateTime($options = [])
    {
        return (new DateTime($options))->create();
    }

    public static function button($options = [])
    {
        return (new Button(array_merge($options, ['type' => 'button'])))->create();
    }

    public static function submit($options = [])
    {
        return (new Button(array_merge($options, ['type' => 'submit'])))->create();
    }

    public static function uploadBtn()
    {

    }

    public static function upload()
    {

    }


    public static function tag($tagName, $options = [])
    {
        return (new Tag($tagName, $options))->create();
    }
}