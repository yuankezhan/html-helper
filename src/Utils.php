<?php

namespace yuankezhan\htmlHelper;

class Utils
{
    /**
     * 获取根目录
     * @return string
     */
    public static function rootPath()
    {
        return dirname(__FILE__);
    }

    /**
     * 判断是否手机
     * @return false
     */
    public static function isMobile()
    {
        return false;
    }

    public static function rootNameSpace()
    {
        return __NAMESPACE__;
    }
}