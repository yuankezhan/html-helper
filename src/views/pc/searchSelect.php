<?= $parentCreate?>

<script>
    $("#<?=$options->id?>").selectPage({
        multiple: <?=$options->multiple ? 1 : 0?>,
        showField: '<?=$options->showField?>',
        keyField: '<?=$options->keyField?>',
        pageSize: <?=$options->pageSize?>,
        data: <?=empty($options->requestUrl) ? json_encode($options->data) : "'{$options->requestUrl}'"?>,
        eAjaxSuccess: function(data) {return data;}
    });
</script>
