<?php

namespace yuankezhan\htmlHelper\html;

class Button extends BaseHtml
{

    protected function resources()
    {
        return ['buttonResources'];
    }

    public function tagName()
    {
        return 'button';
    }

}