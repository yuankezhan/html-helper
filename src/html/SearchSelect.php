<?php


namespace yuankezhan\htmlHelper\html;


class SearchSelect extends Input
{
    protected function optionClass()
    {
        return "{$this->rootNameSpace()}\options\SearchSelectOptions";
    }

    protected function resources()
    {
        return ['searchSelectResources'];
    }

    public function create()
    {
        return $this->render('searchSelect', [
            "options" => $this->options,
            "parentCreate" => parent::create(),
        ]);
    }
}