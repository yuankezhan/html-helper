<?php

namespace yuankezhan\htmlHelper\html;

class DateTime extends Input
{
    protected function optionClass()
    {
        return "{$this->rootNameSpace()}\options\LayDateOptions";
    }

    protected function resources()
    {
        return ['layDateResources'];
    }

    public function create()
    {
        $this->options->onclick = "laydate({istime: true, format: '{$this->options->format}'})";
        return $this->render('dateTime', [
            'input' => parent::create(),
        ]);
    }
}