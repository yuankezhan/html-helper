<?php

namespace yuankezhan\htmlHelper\html;

use yuankezhan\htmlHelper\options\KeyValuePair;

class Select extends BaseHtml
{
    public function tagName()
    {
        return 'select';
    }

    protected function optionClass()
    {
        return "{$this->rootNameSpace()}\options\SelectOptions";
    }

    public function create()
    {
        $itemStr = "";
        foreach ($this->options->items as $item)
        {
            /**
             * @var KeyValuePair $item 下拉菜单的选项
             */
            $itemStr .= (new Tag('option', ['content' => $item->value, 'value' => $item->key]))->create();
        }
        $this->options->content = $itemStr;
        return parent::create();
    }
}