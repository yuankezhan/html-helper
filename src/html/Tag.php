<?php

namespace yuankezhan\htmlHelper\html;

/**
 * 公共的生成任意标签的类
 */
class Tag extends BaseHtml
{
    public $name;
    public function __construct($name, $options = [])
    {
        $this->name = $name;
        parent::__construct($options);
    }

    public function tagName()
    {
        return $this->name;
    }
}