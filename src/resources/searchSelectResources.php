<script>
    function loadjscssfile(filename) {
        var fileref = document.createElement('script');
        fileref.setAttribute("type", "text/javascript");
        fileref.setAttribute("src", filename);
        document.getElementsByTagName("head")[0].appendChild(fileref);
    }

    if (typeof jQuery == 'undefined') {
        // jQuery 未加载
        loadjscssfile("https://www.jpgoodbuy.com/yii/vendor/bower-asset/jquery/dist/jquery.min.js");
    }
</script>

<style>
    /**
     * Container
     */
    .sp_container {
        border: none;
        margin: 0;
        padding: 0;
        display: inline-block;
        position: relative;
        vertical-align: middle;
    }

    .sp_input {
        background-color: white;

        border: 1px solid #ccc;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;

        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        margin: 0 !important;
        /* width: 320px; */


        font-size: 14px;
        height: 34px;
        line-height: 34px;
        min-height: 34px;
        padding: 4px 6px;
        vertical-align: middle;
        display: block;
        width: 100%;
        outline: none;
        box-sizing: border-box;
    }

    .sp_input:focus {
        border: 1px solid #ccc;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    }


    .sp_container_open .sp_input {
        border-radius: 2px !important;
    }

    .sp_container_open .sp_button span.sp_caret {
        transform: rotate(180deg);
    }

    /**
     * "Get all" button
     */
    div.sp_button {
        display: inline-block;
        border-bottom-right-radius: 0;
        border-top-right-radius: 0;
        cursor: pointer;
        text-align: center;
        box-sizing: border-box;

        border: 0;
        width: 24px;
        height: 100%;
        padding: 0;
        vertical-align: middle;
        line-height: 100%;
        position: absolute;
        top: 0;
        right: 0;
    }

    .sp_container_open > .sp_button {
        border-bottom-right-radius: 0 !important;
    }

    div.sp_button span.sp_caret {
        position: absolute;
        top: 50%;
        right: 12px;
        margin-top: -1px;
        vertical-align: middle;
        display: inline-block;
        width: 0;
        height: 0;
        margin-left: 2px;
        /*vertical-align: middle;*/
        border-top: 4px dashed;
        /*border-top: 4px solid\9;*/
        border-right: 4px solid transparent;
        border-left: 4px solid transparent;
        transition: transform .2s ease;
    }

    div.sp_clear_btn {
        position: absolute;
        top: 0;
        right: 25px;
        display: block;
        width: auto;
        height: 100%;
        cursor: pointer;
        font-size: 20px;
        color: #666666;
        font-weight: 600;
        margin: 0;
        padding: 6px 0 0 0;
        box-sizing: border-box;
        line-height: 1;
        font-family: "Helvetica Neue Light", "HelveticaNeue-Light", "Helvetica Neue", Calibri, Helvetica, Arial;
    }

    div.sp_clear_btn:hover {
        color: black;
        font-weight: bold;
    }

    div.sp_clear_btn i {
        font-size: 14px;
    }

    div.sp_clear_btn.sp_align_right {
        right: 10px;
    }

    /**
     * Results
     */
    .sp_result_area {
        /* background-color: transparent; */
        background-color: white;
        border: 1px solid #D6D7D7;
        display: none;
        list-style: none;
        margin: 0;
        padding: 0;
        position: absolute;
        z-index: 100;
        width: 300px;
        border-radius: 2px;
    }

    div.sp_result_area.shadowDown {
        box-shadow: 0 3px 12px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 3px 12px rgba(0, 0, 0, 0.2);
        -webkit-box-shadow: 0 3px 12px rgba(0, 0, 0, 0.2);

        -webkit-animation: dropDownFadeInDown 200ms cubic-bezier(.23, 1, .32, 1);
        animation: dropDownFadeInDown 200ms cubic-bezier(.23, 1, .32, 1);
    }

    div.sp_result_area.shadowUp {
        box-shadow: 0 -1px 12px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 -1px 12px rgba(0, 0, 0, 0.2);
        -webkit-box-shadow: 0 -1px 12px rgba(0, 0, 0, 0.2);

        -webkit-animation: dropDownFadeInUp 300ms cubic-bezier(.23, 1, .32, 1);
        animation: dropDownFadeInUp 300ms cubic-bezier(.23, 1, .32, 1);
    }

    .sp_results {
        background-color: white;
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .sp_results > li {
        height: auto !important;
        line-height: 1;
        margin: 0;
        overflow: hidden;
        padding: 4px 8px;
        position: relative;
        text-align: left;
        white-space: nowrap;
        font-size: 14px;
        color: black;
        cursor: pointer;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    }

    .sp_results > li.sp_message_box {
        height: 30px;
        line-height: 30px;
        text-align: center;
        box-sizing: content-box;
        font-size: 14px;
        cursor: default;
    }

    ul.sp_results > li.sp_over {
        background-color: #53A4EA !important;
        color: #fff !important;
        cursor: pointer;
    }

    ul.sp_results > li.sp_selected {
        color: #cccccc;
        cursor: default;
    }

    .sp_control_box {
        padding: 0;
        height: 27px;
    }

    .sp_control_box p {
        margin: 0;
        line-height: 27px;
        padding-left: 8px;
        font-size: 14px;
        font-weight: 600;
    }

    .sp_control_box button {
        height: 27px;
        line-height: 20px;
        -webkit-border-radius: 0;
        -moz-border-radius: 0;
        border-radius: 0;
        font-size: 13px !important;
        padding: 0 12px;
        border: 0;
        background: white none;
        color: #666666;
        text-align: left;
        -webkit-transition: all .5s cubic-bezier(.175, .885, .32, 1);
        transition: all .5s cubic-bezier(.175, .885, .32, 1);
        outline: none !important;
        float: right;
        opacity: .3;
    }

    .sp_control_box:hover {
        background-color: #F8F8F8;
    }

    .sp_control_box:hover button {
        background-color: #F8F8F8;
        opacity: 1;
    }

    .sp_control_box:hover button:hover {
        background-color: #EEEEEE;
        color: black;
    }

    /**
     * 多选模式相关样式
     */
    div.sp_container_combo {
        border: 1px solid #CCCCCC;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-sizing: border-box;
    }

    /* 多选模式的禁用状态样式 */
    div.sp_container_combo.sp_disabled {
        box-shadow: none;
    }

    div.sp_container_combo.sp_disabled,
    div.sp_container_combo.sp_disabled ul.sp_element_box,
    div.sp_container_combo.sp_disabled .sp_input,
    div.sp_container.sp_disabled .sp_input {
        background-color: #eeeeee;
        cursor: not-allowed !important;
    }

    div.sp_container.sp_disabled .sp_clear_btn,
    div.sp_container_combo.sp_disabled ul.sp_element_box span.tag_close {
        display: none;
    }

    div.sp_container_combo.sp_container_open {
        border-radius: 2px;
    }

    input.sp_combo_input {
        border: 0 !important;
        box-shadow: none;
        background-color: transparent;
        max-width: 100%;
        padding: 0;
        height: 34px !important;
        line-height: 34px !important;
        min-height: 34px !important;
    }

    input.sp_combo_input:focus {
        box-shadow: none;
        border: 0;
    }

    input.sp_combo_input[readonly],
    input.sp_combo_input[disabled] {
        background-color: white;
    }

    ul.sp_element_box {
        margin: 0;
        padding: 3px 0 0 3px;
        position: relative;
        overflow: hidden;
        clear: both;
        cursor: text;
        margin-right: 24px;
        list-style: none;
        height: auto;
        min-height: 25px;
        font-size: 14px;
    }

    ul.sp_element_box > li {
        list-style: none;
        padding: 0 5px;
        margin-right: 3px;
        margin-bottom: 3px;
        /* margin-bottom: 2px; */
        float: left;
        position: relative;
        box-sizing: content-box;
    }

    ul.sp_element_box li.full_width {
        width: 100%;
    }

    ul.sp_element_box li.full_width input {
        width: 100% !important;
    }

    ul.sp_element_box li.selected_tag {
        border: 1px solid #AAAAAA;
        border-radius: 3px;
        background-color: #EFEFEF;
        cursor: pointer;
        max-width: 100%;
        box-shadow: 0 0 2px white inset, 0 1px 0 rgba(0, 0, 0, 0.05);
        height: 24px;
        line-height: 24px;
        -webkit-transition: all .5s cubic-bezier(.175, .885, .32, 1);
        transition: all .5s cubic-bezier(.175, .885, .32, 1);
    }

    ul.sp_element_box li.selected_tag:hover {
        background-color: white;
        border: 1px solid #D0D0D0;
        box-shadow: 0 2px 7px rgba(0, 0, 0, .1);
    }

    ul.sp_element_box li.selected_tag i {
        font-size: 14px;
        color: #AAAAAA;
    }

    ul.sp_element_box li.selected_tag i:hover {
        color: black;
    }

    ul.sp_element_box li.input_box {
        padding: 0;
        /* margin: 0; */
        margin-top: 0;
        height: 26px;
        min-height: 26px;
    }

    ul.sp_element_box li.input_box input {
        height: 26px !important;
        line-height: 26px !important;
        min-height: 26px !important;
    }

    ul.sp_element_box li.selected_tag span.tag_close {
        cursor: pointer;
        margin-left: 5px;
        font-size: 15px;
        font-family: "Helvetica Neue Light", "HelveticaNeue-Light", "Helvetica Neue", Calibri, Helvetica, Arial;
    }


    /**
     * 加深颜色表示非选中
     */
    .sp_results_off {
        /*background: rgba(255, 255, 255, 0.8);*/
    }

    .sp_input_off {
        background: #eee;
        color: #333333;
    }

    .sp_hide {
        display: none;
    }

    /**
     * Navi
     */
    .sp_navi {
        background: #eee;
        border-bottom: 1px solid #79b;
        font-size: 13px;
        font-weight: normal;
        line-height: 1;
        margin: 0;
        padding: 4px;
        text-align: right;
    }

    .sp_navi > p > a:link,
    .sp_navi > p > a:visited,
    .sp_navi > p > a:hover,
    .sp_navi > p > a:active {
        color: blue;
        font-weight: normal;
        margin: 0 4px;
        text-decoration: underline;
    }

    .sp_navi > p {
        color: black;
        font-size: 15px;
        margin: 0;
        padding-top: 4px;
        text-align: center;
    }

    .sp_navi > p > a > .current {
        color: #00c;
        font-size: 16px;
        font-weight: bold;
    }

    .sp_navi > p > .page_end {
        color: gray;
        font-weight: normal;
        margin: 0 4px;
    }

    .navi_page,
    .navi_first,
    .navi_prev,
    .navi_next,
    .navi_last {
        margin: auto 4px !important;
    }

    .sp_navi > .info {
        margin: 0 !important;
        padding: 0 !important;
    }

    /**
     * Select only
     */
    .sp_container > .sp_select_ng {
        background: #fcc;
    }


    /*输入框设置了input-block-level样式时的特殊情况修复*/
    div.sp_container input.sp_input.input-block-level {
        box-sizing: border-box;
        height: 30px;
        line-height: 30px;
        min-height: 30px;
        width: 100%;
    }

    div.sp_container_open .sp_input::-ms-clear {
        display: none;
    }

    /* 移除微软浏览器，在输入框输入文本后，会出现X的问题，but is look like not working */
    input::-ms-clear {
        display: none;
    }

    /*隐藏文本框叉子*/
    input::-ms-reveal {
        display: none;
    }

    /*隐藏密码框小眼睛*/

    div.sp_navi > p {
        font-size: 12px;
    }


    /**
     * 分页条样式
     */
    div.sp_result_area div.sp_pagination {
        margin: 0;
        padding: 0;
        height: 26px;
        line-height: 26px;
        width: 100%;
        /* border-top: 1px solid #DDDDDD; */
        -webkit-border-radius: 0;
        -moz-border-radius: 0;
        border-radius: 0;
        border-bottom-left-radius: 2px;
        border-bottom-right-radius: 2px;
        display: block;
    }

    div.sp_result_area div.sp_pagination ul {
        width: 100%;
        display: inline-block;
        margin: 0;
        -webkit-border-radius: 0;
        -moz-border-radius: 0;
        border-radius: 0;
        border-bottom-left-radius: 2px;
        border-bottom-right-radius: 2px;
        /*     -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,0.05);
            box-shadow: 0 1px 2px rgba(0,0,0,0.05); */
        padding: 0;
    }

    div.sp_result_area div.sp_pagination li {
        text-align: center;
        display: inline;
        box-sizing: border-box;
    }

    div.sp_result_area div.sp_pagination > ul > li > a {
        margin: 0;
        /* border: 1px solid #ddd; */
        border-radius: 0;
        padding: 0;
        box-shadow: none;
        -moz-box-shadow: none;
        -webkit-box-shadow: none;
        background-color: white;

        float: left;
        text-decoration: none;

        border: 0;
        box-sizing: content-box;
        color: #666666;
        font-size: 20px;
        height: 26px;
        line-height: 26px;
        -webkit-transition: all .5s cubic-bezier(.175, .885, .32, 1);
        transition: all .5s cubic-bezier(.175, .885, .32, 1);
    }

    div.sp_result_area div.sp_pagination li.csFirstPage a,
    div.sp_result_area div.sp_pagination li.csPreviousPage a,
    div.sp_result_area div.sp_pagination li.csNextPage a,
    div.sp_result_area div.sp_pagination li.csLastPage a {
        width: 30px;
    }

    div.sp_result_area div.sp_pagination li.csFirstPage a {
        border-left: 0;
        border-bottom-left-radius: 2px;
    }

    div.sp_result_area div.sp_pagination li.csLastPage a {
        border-right: 0;
        border-bottom-right-radius: 2px;
    }

    div.sp_result_area div.sp_pagination > ul > li > a:hover {
        color: #000000;
        background-color: #E8E8E8;
    }

    div.sp_result_area div.sp_pagination > ul > li.disabled > a {
        color: #DDDDDD;
        cursor: default;
    }

    div.sp_result_area div.sp_pagination > ul > li.disabled > a:hover {
        color: #DDDDDD;
        background-color: white;
    }

    div.sp_result_area div.sp_pagination > ul > li.pageInfoBox > a {
        width: 178px;
        text-align: center;
        /* padding-left: 4px;
        padding-right: 4px; */
        color: #666666;
        font-size: 14px;
    }

    div.sp_result_area div.sp_pagination > ul > li.pageInfoBox > a:hover {
        background-color: inherit;
        color: #666666;
        cursor: default;
    }


    @keyframes dropDownFadeInDown {
        from {
            opacity: 0;
            transform: translate3d(0, -20px, 0);
        }
        to {
            opacity: 1;
            transform: translate3d(0, 0, 0);
        }
    }

    @keyframes dropDownFadeInUp {
        from {
            opacity: 0;
            transform: translate3d(0, 20px, 0);
        }
        to {
            opacity: 1;
            transform: translate3d(0, 0, 0);
        }
    }

    /* icons */
    @font-face {
        font-family: "sp-iconfont";
        src: url('data:image/eot;base64,zA0AACQNAAABAAIAAAAAAAIABQMAAAAAAAABAJABAAAAAExQAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAfJwy7wAAAAAAAAAAAAAAAAAAAAAAABAAaQBjAG8AbgBmAG8AbgB0AAAADgBSAGUAZwB1AGwAYQByAAAAFgBWAGUAcgBzAGkAbwBuACAAMQAuADAAAAAQAGkAYwBvAG4AZgBvAG4AdAAAAAAAAAEAAAALAIAAAwAwR1NVQrD+s+0AAAE4AAAAQk9TLzJW7kj9AAABfAAAAFZjbWFwNbM7dAAAAgAAAAI2Z2x5ZtLBAFwAAARQAAAFxGhlYWQPYaJpAAAA4AAAADZoaGVhB94DjQAAALwAAAAkaG10eCvqAAAAAAHUAAAALGxvY2EIbAmwAAAEOAAAABhtYXhwARoAeAAAARgAAAAgbmFtZT5U/n0AAAoUAAACbXBvc3ScItv9AAAMhAAAAKAAAQAAA4D/gABcBAEAAAAABAAAAQAAAAAAAAAAAAAAAAAAAAsAAQAAAAEAAO8ynHxfDzz1AAsEAAAAAADWIS8pAAAAANYhLykAAP/ABAADWgAAAAgAAgAAAAAAAAABAAAACwBsAAUAAAAAAAIAAAAKAAoAAAD/AAAAAAAAAAEAAAAKAB4ALAABREZMVAAIAAQAAAAAAAAAAQAAAAFsaWdhAAgAAAABAAAAAQAEAAQAAAABAAgAAQAGAAAAAQAAAAAAAQP+AZAABQAIAokCzAAAAI8CiQLMAAAB6wAyAQgAAAIABQMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUGZFZABAAHjnDQOA/4AAXAOAAIAAAAABAAAAAAAABAAAAAPpAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQBAAAAAAAFAAAAAwAAACwAAAAEAAABrgABAAAAAACoAAMAAQAAACwAAwAKAAABrgAEAHwAAAAWABAAAwAGAHjmAOYo5jTmPOZa5n7mgeaM5w3//wAAAHjmAOYo5jTmPOZa5n7mgeaM5w3//wAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAFgAWABYAFgAWABYAFgAWABYAFgAAAAEACgAEAAIACAAJAAUABgADAAcAAAEGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAAAIgAAAAAAAAACgAAAHgAAAB4AAAAAQAA5gAAAOYAAAAACgAA5igAAOYoAAAABAAA5jQAAOY0AAAAAgAA5jwAAOY8AAAACAAA5loAAOZaAAAACQAA5n4AAOZ+AAAABQAA5oEAAOaBAAAABgAA5owAAOaMAAAAAwAA5w0AAOcNAAAABwAAAAAAAAB2ALABQgGIAcgCCAIsAo4CwALiAAUAAP/hA7wDGAATACgAMQBEAFAAAAEGKwEiDgIdASEnNC4CKwEVIQUVFxQOAycjJyEHIyIuAz0BFyIGFBYyNjQmFwYHBg8BDgEeATMhMjYnLgInATU0PgI7ATIWHQEBGRsaUxIlHBIDkAEKGCcehf5KAqIBFR8jHA8+Lf5JLD8UMiATCHcMEhIZEhKMCAYFBQgCAgQPDgFtFxYJBQkKBv6kBQ8aFbwfKQIfAQwZJxpMWQ0gGxJhiDRuHSUXCQEBgIABExsgDqc/ERoRERoRfBoWExIZBxANCBgaDSMkFAF35AsYEwwdJuMAAAAAAgAA/8ADwANAAA8AIAAAATIWFxEOASMhIiYnET4BMyUhDgEHER4BFyE+ATcRLgEnA1QYHwEBHxj9WBggAQEgGAKo/VguPQEBPS4CqC09AQE9LQMMIBj9WBggIBgCqBggMwE8Lv1YLjwBATwuAqguPAEAAAADAAD/xgOhAzoAKQBDAGsAAAEhNS4BJyMOAQcVISIGHQEUFhczFQ4BBx4BFyE+ATcuASc1Mz4BPQE0JgcUBiMhIiY9ATQ2MyE1PgE7ATIWFxUhMhYVAyMiBh0BIzU0JisBIgYdASM1NCYrASIGBxUjPgE3NSEVHgEXIzU0JgOA/uoBLiMwIy4B/uoNExMNEQEPAQESDgK+DhIBAQ8BEQ0TExgJB/02BwkJBwEmASAYDBggAQEmBwnGCAUHgwcFCAUHiwcFCAUHAaABDgECiAEOAZgHAjuvIi0BAS0irxIOnw0SAfY2SQENEgEBEg0CSzbzARINnw4SpQcJCQdrBwnRFxcXF9EJB/7EBwXJyQUHBwXJyQUHBwXJAT829vY2PwHJBQcABAAA/9EDsQM2AA8AHwAjACcAABM+ATMhMhYXEQ4BByEuAScTER4BMyEyNjURNCYjISIGASc3FycBFwFNASwhAschLQEBLSH9OSEsATsBFQ8CoxAVFRD9XQ8VAQjgKeBPAbYr/ksC5yEtLSH9OSEsAQEsIQK1/V0QFRUQAqMQFRX9q98p3wkBtSv+SgACAAD/wAOBA0AAEAAiAAABNjQnASYiBhQXCQEGFBYyNxMUFwkBBhQWMjcBNjQnASYiBgJWCQn+YAkbEgkBif53CRIbCesJAYn+dwkSGwkBoAkJ/mAJGxIBaQoaCgGgCRMaCf52/nYJGhMJA1cNCf52/nYJGhMJAaAKGgoBoAkTAAIAAP/AA4EDQAAQACIAAAEGFBcBFjI2NCcJATY0JiIHAzQnCQE2NCYiBwEGFBcBFjI2AaoJCQGgCRsSCf53AYkJEhsJ6wn+dwGJCRIbCf5gCQkBoAkbEgGXChoK/mAJExoJAYoBigkaEwn8qQ0JAYoBigkaEwn+YAoaCv5gCRMAAQAA/+YC2gMaABAAAAkBJiIGFBcJAQYUFjI3ATY0AtL+gAgUEAgBbv6SCBAUCAGACAGSAYAIEBQI/pL+kggUEAgBgAgUAAAABQAAAAADwQNaABEAHgArACwAOQAAJSEiLgE2NwE+ATIWFwEeAQ4BAQYHAQYWMyEyNicBJgMiJjURPgEyFhURFAYHIxQeATI+ATQuASIOAQNQ/WAiMxsDEAFTES41LxEBUxADGzP+jhIQ/q0QFCECoCEUEP6tEBINEwERHBITDS8MFxgXDAwXGBcMHhkvOR4CXh4gIB79oR05LxkC/AEd/aEdIiIdAl8d/iQTDQEfDRISDf7hDRNtDBYNDRYZFg0NFgAAAAIAAP/BAsUDOgAMABkAAAEiJjQ3ATYyFhQHAQYBJicBJjQ2MhcBFhQGAQkKDQYBpQcTDgf+WwcBmwkH/lsGDhIHAaUHDgFmDhIIAaQHDhIH/lsH/lsBBgGlBxMNBv5bBxMNAAABAAD/9AOMAwwACwAAJQcJAScJATcJARcBA4xQ/sT+xFABPf7DUAE8ATxQ/sNEUAE9/sNQATwBPFD+wwE9UP7EAAAAAAAAEgDeAAEAAAAAAAAAFQAAAAEAAAAAAAEACAAVAAEAAAAAAAIABwAdAAEAAAAAAAMACAAkAAEAAAAAAAQACAAsAAEAAAAAAAUACwA0AAEAAAAAAAYACAA/AAEAAAAAAAoAKwBHAAEAAAAAAAsAEwByAAMAAQQJAAAAKgCFAAMAAQQJAAEAEACvAAMAAQQJAAIADgC/AAMAAQQJAAMAEADNAAMAAQQJAAQAEADdAAMAAQQJAAUAFgDtAAMAAQQJAAYAEAEDAAMAAQQJAAoAVgETAAMAAQQJAAsAJgFpCkNyZWF0ZWQgYnkgaWNvbmZvbnQKaWNvbmZvbnRSZWd1bGFyaWNvbmZvbnRpY29uZm9udFZlcnNpb24gMS4waWNvbmZvbnRHZW5lcmF0ZWQgYnkgc3ZnMnR0ZiBmcm9tIEZvbnRlbGxvIHByb2plY3QuaHR0cDovL2ZvbnRlbGxvLmNvbQAKAEMAcgBlAGEAdABlAGQAIABiAHkAIABpAGMAbwBuAGYAbwBuAHQACgBpAGMAbwBuAGYAbwBuAHQAUgBlAGcAdQBsAGEAcgBpAGMAbwBuAGYAbwBuAHQAaQBjAG8AbgBmAG8AbgB0AFYAZQByAHMAaQBvAG4AIAAxAC4AMABpAGMAbwBuAGYAbwBuAHQARwBlAG4AZQByAGEAdABlAGQAIABiAHkAIABzAHYAZwAyAHQAdABmACAAZgByAG8AbQAgAEYAbwBuAHQAZQBsAGwAbwAgAHAAcgBvAGoAZQBjAHQALgBoAHQAdABwADoALwAvAGYAbwBuAHQAZQBsAGwAbwAuAGMAbwBtAAAAAAIAAAAAAAAACgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwECAQMBBAEFAQYBBwEIAQkBCgELAQwAAXgFY2hlY2sKaWNvbi1jbGVhcgl4dWFuemhvbmcUaWNvbi1kaXJlY3Rpb24tcmlnaHQTaWNvbi1kaXJlY3Rpb24tbGVmdAR5b3UxB2ppbmdnYW8Lbm92aWdvX2xlZnQFdGltZXMAAA=='); /* IE9*/
        src: url('data:image/eot;base64,zA0AACQNAAABAAIAAAAAAAIABQMAAAAAAAABAJABAAAAAExQAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAfJwy7wAAAAAAAAAAAAAAAAAAAAAAABAAaQBjAG8AbgBmAG8AbgB0AAAADgBSAGUAZwB1AGwAYQByAAAAFgBWAGUAcgBzAGkAbwBuACAAMQAuADAAAAAQAGkAYwBvAG4AZgBvAG4AdAAAAAAAAAEAAAALAIAAAwAwR1NVQrD+s+0AAAE4AAAAQk9TLzJW7kj9AAABfAAAAFZjbWFwNbM7dAAAAgAAAAI2Z2x5ZtLBAFwAAARQAAAFxGhlYWQPYaJpAAAA4AAAADZoaGVhB94DjQAAALwAAAAkaG10eCvqAAAAAAHUAAAALGxvY2EIbAmwAAAEOAAAABhtYXhwARoAeAAAARgAAAAgbmFtZT5U/n0AAAoUAAACbXBvc3ScItv9AAAMhAAAAKAAAQAAA4D/gABcBAEAAAAABAAAAQAAAAAAAAAAAAAAAAAAAAsAAQAAAAEAAO8ynHxfDzz1AAsEAAAAAADWIS8pAAAAANYhLykAAP/ABAADWgAAAAgAAgAAAAAAAAABAAAACwBsAAUAAAAAAAIAAAAKAAoAAAD/AAAAAAAAAAEAAAAKAB4ALAABREZMVAAIAAQAAAAAAAAAAQAAAAFsaWdhAAgAAAABAAAAAQAEAAQAAAABAAgAAQAGAAAAAQAAAAAAAQP+AZAABQAIAokCzAAAAI8CiQLMAAAB6wAyAQgAAAIABQMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUGZFZABAAHjnDQOA/4AAXAOAAIAAAAABAAAAAAAABAAAAAPpAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQBAAAAAAAFAAAAAwAAACwAAAAEAAABrgABAAAAAACoAAMAAQAAACwAAwAKAAABrgAEAHwAAAAWABAAAwAGAHjmAOYo5jTmPOZa5n7mgeaM5w3//wAAAHjmAOYo5jTmPOZa5n7mgeaM5w3//wAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAFgAWABYAFgAWABYAFgAWABYAFgAAAAEACgAEAAIACAAJAAUABgADAAcAAAEGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAAAIgAAAAAAAAACgAAAHgAAAB4AAAAAQAA5gAAAOYAAAAACgAA5igAAOYoAAAABAAA5jQAAOY0AAAAAgAA5jwAAOY8AAAACAAA5loAAOZaAAAACQAA5n4AAOZ+AAAABQAA5oEAAOaBAAAABgAA5owAAOaMAAAAAwAA5w0AAOcNAAAABwAAAAAAAAB2ALABQgGIAcgCCAIsAo4CwALiAAUAAP/hA7wDGAATACgAMQBEAFAAAAEGKwEiDgIdASEnNC4CKwEVIQUVFxQOAycjJyEHIyIuAz0BFyIGFBYyNjQmFwYHBg8BDgEeATMhMjYnLgInATU0PgI7ATIWHQEBGRsaUxIlHBIDkAEKGCcehf5KAqIBFR8jHA8+Lf5JLD8UMiATCHcMEhIZEhKMCAYFBQgCAgQPDgFtFxYJBQkKBv6kBQ8aFbwfKQIfAQwZJxpMWQ0gGxJhiDRuHSUXCQEBgIABExsgDqc/ERoRERoRfBoWExIZBxANCBgaDSMkFAF35AsYEwwdJuMAAAAAAgAA/8ADwANAAA8AIAAAATIWFxEOASMhIiYnET4BMyUhDgEHER4BFyE+ATcRLgEnA1QYHwEBHxj9WBggAQEgGAKo/VguPQEBPS4CqC09AQE9LQMMIBj9WBggIBgCqBggMwE8Lv1YLjwBATwuAqguPAEAAAADAAD/xgOhAzoAKQBDAGsAAAEhNS4BJyMOAQcVISIGHQEUFhczFQ4BBx4BFyE+ATcuASc1Mz4BPQE0JgcUBiMhIiY9ATQ2MyE1PgE7ATIWFxUhMhYVAyMiBh0BIzU0JisBIgYdASM1NCYrASIGBxUjPgE3NSEVHgEXIzU0JgOA/uoBLiMwIy4B/uoNExMNEQEPAQESDgK+DhIBAQ8BEQ0TExgJB/02BwkJBwEmASAYDBggAQEmBwnGCAUHgwcFCAUHiwcFCAUHAaABDgECiAEOAZgHAjuvIi0BAS0irxIOnw0SAfY2SQENEgEBEg0CSzbzARINnw4SpQcJCQdrBwnRFxcXF9EJB/7EBwXJyQUHBwXJyQUHBwXJAT829vY2PwHJBQcABAAA/9EDsQM2AA8AHwAjACcAABM+ATMhMhYXEQ4BByEuAScTER4BMyEyNjURNCYjISIGASc3FycBFwFNASwhAschLQEBLSH9OSEsATsBFQ8CoxAVFRD9XQ8VAQjgKeBPAbYr/ksC5yEtLSH9OSEsAQEsIQK1/V0QFRUQAqMQFRX9q98p3wkBtSv+SgACAAD/wAOBA0AAEAAiAAABNjQnASYiBhQXCQEGFBYyNxMUFwkBBhQWMjcBNjQnASYiBgJWCQn+YAkbEgkBif53CRIbCesJAYn+dwkSGwkBoAkJ/mAJGxIBaQoaCgGgCRMaCf52/nYJGhMJA1cNCf52/nYJGhMJAaAKGgoBoAkTAAIAAP/AA4EDQAAQACIAAAEGFBcBFjI2NCcJATY0JiIHAzQnCQE2NCYiBwEGFBcBFjI2AaoJCQGgCRsSCf53AYkJEhsJ6wn+dwGJCRIbCf5gCQkBoAkbEgGXChoK/mAJExoJAYoBigkaEwn8qQ0JAYoBigkaEwn+YAoaCv5gCRMAAQAA/+YC2gMaABAAAAkBJiIGFBcJAQYUFjI3ATY0AtL+gAgUEAgBbv6SCBAUCAGACAGSAYAIEBQI/pL+kggUEAgBgAgUAAAABQAAAAADwQNaABEAHgArACwAOQAAJSEiLgE2NwE+ATIWFwEeAQ4BAQYHAQYWMyEyNicBJgMiJjURPgEyFhURFAYHIxQeATI+ATQuASIOAQNQ/WAiMxsDEAFTES41LxEBUxADGzP+jhIQ/q0QFCECoCEUEP6tEBINEwERHBITDS8MFxgXDAwXGBcMHhkvOR4CXh4gIB79oR05LxkC/AEd/aEdIiIdAl8d/iQTDQEfDRISDf7hDRNtDBYNDRYZFg0NFgAAAAIAAP/BAsUDOgAMABkAAAEiJjQ3ATYyFhQHAQYBJicBJjQ2MhcBFhQGAQkKDQYBpQcTDgf+WwcBmwkH/lsGDhIHAaUHDgFmDhIIAaQHDhIH/lsH/lsBBgGlBxMNBv5bBxMNAAABAAD/9AOMAwwACwAAJQcJAScJATcJARcBA4xQ/sT+xFABPf7DUAE8ATxQ/sNEUAE9/sNQATwBPFD+wwE9UP7EAAAAAAAAEgDeAAEAAAAAAAAAFQAAAAEAAAAAAAEACAAVAAEAAAAAAAIABwAdAAEAAAAAAAMACAAkAAEAAAAAAAQACAAsAAEAAAAAAAUACwA0AAEAAAAAAAYACAA/AAEAAAAAAAoAKwBHAAEAAAAAAAsAEwByAAMAAQQJAAAAKgCFAAMAAQQJAAEAEACvAAMAAQQJAAIADgC/AAMAAQQJAAMAEADNAAMAAQQJAAQAEADdAAMAAQQJAAUAFgDtAAMAAQQJAAYAEAEDAAMAAQQJAAoAVgETAAMAAQQJAAsAJgFpCkNyZWF0ZWQgYnkgaWNvbmZvbnQKaWNvbmZvbnRSZWd1bGFyaWNvbmZvbnRpY29uZm9udFZlcnNpb24gMS4waWNvbmZvbnRHZW5lcmF0ZWQgYnkgc3ZnMnR0ZiBmcm9tIEZvbnRlbGxvIHByb2plY3QuaHR0cDovL2ZvbnRlbGxvLmNvbQAKAEMAcgBlAGEAdABlAGQAIABiAHkAIABpAGMAbwBuAGYAbwBuAHQACgBpAGMAbwBuAGYAbwBuAHQAUgBlAGcAdQBsAGEAcgBpAGMAbwBuAGYAbwBuAHQAaQBjAG8AbgBmAG8AbgB0AFYAZQByAHMAaQBvAG4AIAAxAC4AMABpAGMAbwBuAGYAbwBuAHQARwBlAG4AZQByAGEAdABlAGQAIABiAHkAIABzAHYAZwAyAHQAdABmACAAZgByAG8AbQAgAEYAbwBuAHQAZQBsAGwAbwAgAHAAcgBvAGoAZQBjAHQALgBoAHQAdABwADoALwAvAGYAbwBuAHQAZQBsAGwAbwAuAGMAbwBtAAAAAAIAAAAAAAAACgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwECAQMBBAEFAQYBBwEIAQkBCgELAQwAAXgFY2hlY2sKaWNvbi1jbGVhcgl4dWFuemhvbmcUaWNvbi1kaXJlY3Rpb24tcmlnaHQTaWNvbi1kaXJlY3Rpb24tbGVmdAR5b3UxB2ppbmdnYW8Lbm92aWdvX2xlZnQFdGltZXMAAA==') format('embedded-opentype'), /* IE6-IE8 */ url('data:application/x-font-woff;charset=utf-8;base64,d09GRgABAAAAAAi8AAsAAAAADSQAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABHU1VCAAABCAAAADMAAABCsP6z7U9TLzIAAAE8AAAARAAAAFZW7kj9Y21hcAAAAYAAAACoAAACNjWzO3RnbHlmAAACKAAABDQAAAXE0sEAXGhlYWQAAAZcAAAALwAAADYPYaJpaGhlYQAABowAAAAeAAAAJAfeA41obXR4AAAGrAAAABcAAAAsK+oAAGxvY2EAAAbEAAAAGAAAABgIbAmwbWF4cAAABtwAAAAfAAAAIAEaAHhuYW1lAAAG/AAAAUUAAAJtPlT+fXBvc3QAAAhEAAAAdQAAAKCcItv9eJxjYGRgYOBikGPQYWB0cfMJYeBgYGGAAJAMY05meiJQDMoDyrGAaQ4gZoOIAgCKIwNPAHicY2Bk/sc4gYGVgYOpk+kMAwNDP4RmfM1gxMjBwMDEwMrMgBUEpLmmMDgwVDznZW7438AQw9zA0AAUZgTJAQAmWQyAeJzFkjEOgkAQRf8KLkoksfIMFJaUnISTUBgSOgsOYuWxPsfAPzs0Bm11Nm+T+ZvMTP4sgD2ATFxFDoQnAiweUkPSM5RJz3FTfsFZSkRPsGbDlh0Hjpzmaln0/k3fRlCt7TG9VK8dDjhquqhuhcT4scZPIvyv9Xuc0n1fM20F/YpGJBzTWTu2UTaOHAVbR96CnSOXwcGx38DRkfPg5NgPmSsHxQsreTV9eJxlVE2MFEUUrq+K6Zpepnum/6bnh/npbrbbze7O6vxtVnZnTeACaNxkjaIbdL2YGAJH9uJhjTEBJMZw0cQDi0JEjSEe8EQMBMRwxKMXIBpN8KjhxBS+HoaNxE7n1ffqe11f1Xuvi2UYe3RPXBEl5rBn2HNsH1thDNo0QpPXECTdFp+GF2Q83zVFEiWBjMKWWIIfam6x3e/Gvia1PEzU0Qna/aTFE/S6A74H7WINKFcrq/bkLlt8golSUv9QHeBfwGtEu/KDWbV/ZtltN53sRs62y7Z9OqtlMlnOd+RNHPWLekaf0NT5TL7iXWlM8QZy5aTy4utGs2q/faJ7rDbp68DmJpxq0/xq2apY9L5XKTp2WRaMbKliRLtdbPy+s+TkavFvjB5OZ70qrooXWJ416ZTtom+ZiIIwTqwBOpOBCWnV4QcDzFstJOKVUgNolIaHSk2gWeIXh4daS8BSi1+cTcdZkWuO2JQrNTtYbFHEImjkF2kkTUGaN8U58TybYnvZEVINerR0RFJeEGo1uEW/45E31iWu1xlgCd1Yulq6N4L9TtAbIE2p7wXtoiei9Muo142pTv9B0otoiV7g0WLpnNhU99GKno1aUPcNxzEs5AHb5D+aNghaNFfS5bAvdV0ipiPm0pPGUr+ZzcgPZIbsRyOLLSoxP0HmM8n3XApngdnwkm2eNWw86O8HDbANfrD/Nw1nTftCuuIRqd/26bmtS3VDZm7dyshti+X+gwf9ZZDHdlCObovvRZ/q0mARSxhzBmk7jeojA8qJYz3ur57VjSkpGpJ5P4GPlzAT8J+DdDvBcCGYoSR5ef5lwfMKw8N5D9m7U3dfxg/T6iD/M5gdx6TfXB4eToNGocNv70zd0XF5Wh0Y98j71CMFFlK1+t0EMfU6NVva8PPONnpC8Vd1Xa3rVVvHSbWh21X9r22ErTGHdycqE+Q5FV0dV8f1iqOL14xtjK0x/T99Ukb6nyU66cWhFNvoCYVv9FSH9NUGTj7Wf4JI+zGHT0mAPNLHKZxKNR9+bWxjtT6mGfXsoz/4r6JC+kx/6ugky39Rm1m3kMUxdSZbcLPYzOIMGYLqDE2lFAVQ32fSH05cE28wi9XZNJthC4xNBmEL/XkM0k6mG8MENDpHcXRzIBZh3LNSzrNcTUZuHe0Bui26iiBWhuthpyoKWLVavTkLqwVR7aiP7YL6ruAGfCtwU2QbDqxdtmPM5fySnxuZenluoc7frDeb9eG52sJcmT9EjVAY1vhbNbXbMdAwbNtQ9wznaK5oGMVyasb3xTX+E/27OVamWoRxl5LQLrq0Z8S04W6/TSVwNegThoYL0jGlWpP4nPp9TTNtSVMm3jHtLM5LcolTaxgFGho5jsFG+f5HnBY5tpPyI3VQeed16mxxekXdUDdWsKSur2ARiyvq+r6nPCxRBG3zX8yJDj54nGNgZGBgAOL3RuU28fw2Xxm4WRhA4JqiviaC/n+AhYE5CsjlYGACiQIA93sITQB4nGNgZGBgbvjfwBDDAmQxMLAwMIBpJMANAEcqAnUAAHicY2FgYGB+ycDAwkAAMzIwAAAnpgEWAAAAAAAAdgCwAUIBiAHIAggCLAKOAsAC4nicY2BkYGDgZshhYGUAASYg5gJCBob/YD4DABRyAZMAeJxlj01OwzAQhV/6B6QSqqhgh+QFYgEo/RGrblhUavdddN+mTpsqiSPHrdQDcB6OwAk4AtyAO/BIJ5s2lsffvHljTwDc4Acejt8t95E9XDI7cg0XuBeuU38QbpBfhJto41W4Rf1N2MczpsJtdGF5g9e4YvaEd2EPHXwI13CNT+E69S/hBvlbuIk7/Aq30PHqwj7mXle4jUcv9sdWL5xeqeVBxaHJIpM5v4KZXu+Sha3S6pxrW8QmU4OgX0lTnWlb3VPs10PnIhVZk6oJqzpJjMqt2erQBRvn8lGvF4kehCblWGP+tsYCjnEFhSUOjDFCGGSIyujoO1Vm9K+xQ8Jee1Y9zed0WxTU/3OFAQL0z1xTurLSeTpPgT1fG1J1dCtuy56UNJFezUkSskJe1rZUQuoBNmVXjhF6XNGJPyhnSP8ACVpuyAAAAHicbYnNDsIgEAb3q8Va8Kav0YNPZMiKsFrZBKmpPr1/N+OcJjPU0BdL/3FosEALgyU6rNDDwmFNmA2nwGcrrHngMfjSz5PPj6Q5bj/xICVwlZcVialufuIYjrW967TrTpJj9Oqy3iTq/j1MlUu4Ej0BvEMlfAAAAA==') format('woff'),
        url('data:image/ttf;base64,AAEAAAALAIAAAwAwR1NVQrD+s+0AAAE4AAAAQk9TLzJW7kj9AAABfAAAAFZjbWFwNbM7dAAAAgAAAAI2Z2x5ZtLBAFwAAARQAAAFxGhlYWQPYaJpAAAA4AAAADZoaGVhB94DjQAAALwAAAAkaG10eCvqAAAAAAHUAAAALGxvY2EIbAmwAAAEOAAAABhtYXhwARoAeAAAARgAAAAgbmFtZT5U/n0AAAoUAAACbXBvc3ScItv9AAAMhAAAAKAAAQAAA4D/gABcBAEAAAAABAAAAQAAAAAAAAAAAAAAAAAAAAsAAQAAAAEAAO8ydzxfDzz1AAsEAAAAAADWIS8pAAAAANYhLykAAP/ABAADWgAAAAgAAgAAAAAAAAABAAAACwBsAAUAAAAAAAIAAAAKAAoAAAD/AAAAAAAAAAEAAAAKAB4ALAABREZMVAAIAAQAAAAAAAAAAQAAAAFsaWdhAAgAAAABAAAAAQAEAAQAAAABAAgAAQAGAAAAAQAAAAAAAQP+AZAABQAIAokCzAAAAI8CiQLMAAAB6wAyAQgAAAIABQMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUGZFZABAAHjnDQOA/4AAXAOAAIAAAAABAAAAAAAABAAAAAPpAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQBAAAAAAAFAAAAAwAAACwAAAAEAAABrgABAAAAAACoAAMAAQAAACwAAwAKAAABrgAEAHwAAAAWABAAAwAGAHjmAOYo5jTmPOZa5n7mgeaM5w3//wAAAHjmAOYo5jTmPOZa5n7mgeaM5w3//wAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAFgAWABYAFgAWABYAFgAWABYAFgAAAAEACgAEAAIACAAJAAUABgADAAcAAAEGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAAAIgAAAAAAAAACgAAAHgAAAB4AAAAAQAA5gAAAOYAAAAACgAA5igAAOYoAAAABAAA5jQAAOY0AAAAAgAA5jwAAOY8AAAACAAA5loAAOZaAAAACQAA5n4AAOZ+AAAABQAA5oEAAOaBAAAABgAA5owAAOaMAAAAAwAA5w0AAOcNAAAABwAAAAAAAAB2ALABQgGIAcgCCAIsAo4CwALiAAUAAP/hA7wDGAATACgAMQBEAFAAAAEGKwEiDgIdASEnNC4CKwEVIQUVFxQOAycjJyEHIyIuAz0BFyIGFBYyNjQmFwYHBg8BDgEeATMhMjYnLgInATU0PgI7ATIWHQEBGRsaUxIlHBIDkAEKGCcehf5KAqIBFR8jHA8+Lf5JLD8UMiATCHcMEhIZEhKMCAYFBQgCAgQPDgFtFxYJBQkKBv6kBQ8aFbwfKQIfAQwZJxpMWQ0gGxJhiDRuHSUXCQEBgIABExsgDqc/ERoRERoRfBoWExIZBxANCBgaDSMkFAF35AsYEwwdJuMAAAAAAgAA/8ADwANAAA8AIAAAATIWFxEOASMhIiYnET4BMyUhDgEHER4BFyE+ATcRLgEnA1QYHwEBHxj9WBggAQEgGAKo/VguPQEBPS4CqC09AQE9LQMMIBj9WBggIBgCqBggMwE8Lv1YLjwBATwuAqguPAEAAAADAAD/xgOhAzoAKQBDAGsAAAEhNS4BJyMOAQcVISIGHQEUFhczFQ4BBx4BFyE+ATcuASc1Mz4BPQE0JgcUBiMhIiY9ATQ2MyE1PgE7ATIWFxUhMhYVAyMiBh0BIzU0JisBIgYdASM1NCYrASIGBxUjPgE3NSEVHgEXIzU0JgOA/uoBLiMwIy4B/uoNExMNEQEPAQESDgK+DhIBAQ8BEQ0TExgJB/02BwkJBwEmASAYDBggAQEmBwnGCAUHgwcFCAUHiwcFCAUHAaABDgECiAEOAZgHAjuvIi0BAS0irxIOnw0SAfY2SQENEgEBEg0CSzbzARINnw4SpQcJCQdrBwnRFxcXF9EJB/7EBwXJyQUHBwXJyQUHBwXJAT829vY2PwHJBQcABAAA/9EDsQM2AA8AHwAjACcAABM+ATMhMhYXEQ4BByEuAScTER4BMyEyNjURNCYjISIGASc3FycBFwFNASwhAschLQEBLSH9OSEsATsBFQ8CoxAVFRD9XQ8VAQjgKeBPAbYr/ksC5yEtLSH9OSEsAQEsIQK1/V0QFRUQAqMQFRX9q98p3wkBtSv+SgACAAD/wAOBA0AAEAAiAAABNjQnASYiBhQXCQEGFBYyNxMUFwkBBhQWMjcBNjQnASYiBgJWCQn+YAkbEgkBif53CRIbCesJAYn+dwkSGwkBoAkJ/mAJGxIBaQoaCgGgCRMaCf52/nYJGhMJA1cNCf52/nYJGhMJAaAKGgoBoAkTAAIAAP/AA4EDQAAQACIAAAEGFBcBFjI2NCcJATY0JiIHAzQnCQE2NCYiBwEGFBcBFjI2AaoJCQGgCRsSCf53AYkJEhsJ6wn+dwGJCRIbCf5gCQkBoAkbEgGXChoK/mAJExoJAYoBigkaEwn8qQ0JAYoBigkaEwn+YAoaCv5gCRMAAQAA/+YC2gMaABAAAAkBJiIGFBcJAQYUFjI3ATY0AtL+gAgUEAgBbv6SCBAUCAGACAGSAYAIEBQI/pL+kggUEAgBgAgUAAAABQAAAAADwQNaABEAHgArACwAOQAAJSEiLgE2NwE+ATIWFwEeAQ4BAQYHAQYWMyEyNicBJgMiJjURPgEyFhURFAYHIxQeATI+ATQuASIOAQNQ/WAiMxsDEAFTES41LxEBUxADGzP+jhIQ/q0QFCECoCEUEP6tEBINEwERHBITDS8MFxgXDAwXGBcMHhkvOR4CXh4gIB79oR05LxkC/AEd/aEdIiIdAl8d/iQTDQEfDRISDf7hDRNtDBYNDRYZFg0NFgAAAAIAAP/BAsUDOgAMABkAAAEiJjQ3ATYyFhQHAQYBJicBJjQ2MhcBFhQGAQkKDQYBpQcTDgf+WwcBmwkH/lsGDhIHAaUHDgFmDhIIAaQHDhIH/lsH/lsBBgGlBxMNBv5bBxMNAAABAAD/9AOMAwwACwAAJQcJAScJATcJARcBA4xQ/sT+xFABPf7DUAE8ATxQ/sNEUAE9/sNQATwBPFD+wwE9UP7EAAAAAAAAEgDeAAEAAAAAAAAAFQAAAAEAAAAAAAEACAAVAAEAAAAAAAIABwAdAAEAAAAAAAMACAAkAAEAAAAAAAQACAAsAAEAAAAAAAUACwA0AAEAAAAAAAYACAA/AAEAAAAAAAoAKwBHAAEAAAAAAAsAEwByAAMAAQQJAAAAKgCFAAMAAQQJAAEAEACvAAMAAQQJAAIADgC/AAMAAQQJAAMAEADNAAMAAQQJAAQAEADdAAMAAQQJAAUAFgDtAAMAAQQJAAYAEAEDAAMAAQQJAAoAVgETAAMAAQQJAAsAJgFpCkNyZWF0ZWQgYnkgaWNvbmZvbnQKaWNvbmZvbnRSZWd1bGFyaWNvbmZvbnRpY29uZm9udFZlcnNpb24gMS4waWNvbmZvbnRHZW5lcmF0ZWQgYnkgc3ZnMnR0ZiBmcm9tIEZvbnRlbGxvIHByb2plY3QuaHR0cDovL2ZvbnRlbGxvLmNvbQAKAEMAcgBlAGEAdABlAGQAIABiAHkAIABpAGMAbwBuAGYAbwBuAHQACgBpAGMAbwBuAGYAbwBuAHQAUgBlAGcAdQBsAGEAcgBpAGMAbwBuAGYAbwBuAHQAaQBjAG8AbgBmAG8AbgB0AFYAZQByAHMAaQBvAG4AIAAxAC4AMABpAGMAbwBuAGYAbwBuAHQARwBlAG4AZQByAGEAdABlAGQAIABiAHkAIABzAHYAZwAyAHQAdABmACAAZgByAG8AbQAgAEYAbwBuAHQAZQBsAGwAbwAgAHAAcgBvAGoAZQBjAHQALgBoAHQAdABwADoALwAvAGYAbwBuAHQAZQBsAGwAbwAuAGMAbwBtAAAAAAIAAAAAAAAACgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwECAQMBBAEFAQYBBwEIAQkBCgELAQwAAXgFY2hlY2sKaWNvbi1jbGVhcgl4dWFuemhvbmcUaWNvbi1kaXJlY3Rpb24tcmlnaHQTaWNvbi1kaXJlY3Rpb24tbGVmdAR5b3UxB2ppbmdnYW8Lbm92aWdvX2xlZnQFdGltZXMAAA==') format('truetype'), /* chrome, firefox, opera, Safari, Android, iOS 4.2+*/ url('data:image/svg;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/Pgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiID4KPCEtLQoyMDEzLTktMzA6IENyZWF0ZWQuCi0tPgo8c3ZnPgo8bWV0YWRhdGE+CkNyZWF0ZWQgYnkgaWNvbmZvbnQKPC9tZXRhZGF0YT4KPGRlZnM+Cgo8Zm9udCBpZD0iaWNvbmZvbnQiIGhvcml6LWFkdi14PSIxMDI0IiA+CiAgPGZvbnQtZmFjZQogICAgZm9udC1mYW1pbHk9Imljb25mb250IgogICAgZm9udC13ZWlnaHQ9IjUwMCIKICAgIGZvbnQtc3RyZXRjaD0ibm9ybWFsIgogICAgdW5pdHMtcGVyLWVtPSIxMDI0IgogICAgYXNjZW50PSI4OTYiCiAgICBkZXNjZW50PSItMTI4IgogIC8+CiAgICA8bWlzc2luZy1nbHlwaCAvPgogICAgCiAgICA8Z2x5cGggZ2x5cGgtbmFtZT0ieCIgdW5pY29kZT0ieCIgaG9yaXotYWR2LXg9IjEwMDEiCmQ9Ik0yODEgNTQzcS0yNyAtMSAtNTMgLTFoLTgzcS0xOCAwIC0zNi41IC02dC0zMi41IC0xOC41dC0yMyAtMzJ0LTkgLTQ1LjV2LTc2aDkxMnY0MXEwIDE2IC0wLjUgMzB0LTAuNSAxOHEwIDEzIC01IDI5dC0xNyAyOS41dC0zMS41IDIyLjV0LTQ5LjUgOWgtMTMzdi05N2gtNDM4djk3ek05NTUgMzEwdi01MnEwIC0yMyAwLjUgLTUydDAuNSAtNTh0LTEwLjUgLTQ3LjV0LTI2IC0zMHQtMzMgLTE2dC0zMS41IC00LjVxLTE0IC0xIC0yOS41IC0wLjUKdC0yOS41IDAuNWgtMzJsLTQ1IDEyOGgtNDM5bC00NCAtMTI4aC0yOWgtMzRxLTIwIDAgLTQ1IDFxLTI1IDAgLTQxIDkuNXQtMjUuNSAyM3QtMTMuNSAyOS41dC00IDMwdjE2N2g5MTF6TTE2MyAyNDdxLTEyIDAgLTIxIC04LjV0LTkgLTIxLjV0OSAtMjEuNXQyMSAtOC41cTEzIDAgMjIgOC41dDkgMjEuNXQtOSAyMS41dC0yMiA4LjV6TTMxNiAxMjNxLTggLTI2IC0xNCAtNDhxLTUgLTE5IC0xMC41IC0zN3QtNy41IC0yNXQtMyAtMTV0MSAtMTQuNQp0OS41IC0xMC41dDIxLjUgLTRoMzdoNjdoODFoODBoNjRoMzZxMjMgMCAzNCAxMnQyIDM4cS01IDEzIC05LjUgMzAuNXQtOS41IDM0LjVxLTUgMTkgLTExIDM5aC0zNjh6TTMzNiA0OTh2MjI4cTAgMTEgMi41IDIzdDEwIDIxLjV0MjAuNSAxNS41dDM0IDZoMTg4cTMxIDAgNTEuNSAtMTQuNXQyMC41IC01Mi41di0yMjdoLTMyN3oiIC8+CiAgICAKCiAgICAKICAgIDxnbHlwaCBnbHlwaC1uYW1lPSJjaGVjayIgdW5pY29kZT0iJiM1ODkzMjsiIGQ9Ik04NTEuNjA4IDc4MC4yNzljMzEuMDU2IDAgNTYuMzIzLTI1LjI2NyA1Ni4zMjMtNTYuMzI1di02NzkuODg2YzAtMzEuMDU3LTI1LjI2Ni01Ni4zMjQtNTYuMzIzLTU2LjMyNGgtNjc5Ljg4NmMtMzEuMDU4IDAtNTYuMzI1IDI1LjI2Ni01Ni4zMjUgNTYuMzI0bDAgNjc5Ljg4NmMwIDMxLjA1OCAyNS4yNjcgNTYuMzI1IDU2LjMyNSA1Ni4zMjVoNjc5Ljg4Nk04NTEuNjA4IDgzMS40NDVoLTY3OS44ODZjLTU5LjM2NSAwLTEwNy40OS00OC4xMjUtMTA3LjQ5LTEwNy40OXYtNjc5Ljg4NmMwLTU5LjM2NSA0OC4xMjUtMTA3LjQ4OSAxMDcuNDktMTA3LjQ4OWg2NzkuODg2YzU5LjM2NSAwIDEwNy40ODggNDguMTI0IDEwNy40ODggMTA3LjQ4OXY2NzkuODg2YzAgNTkuMzY1LTQ4LjEyMyAxMDcuNDktMTA3LjQ4OCAxMDcuNDl2MHoiICBob3Jpei1hZHYteD0iMTAyNCIgLz4KCiAgICAKICAgIDxnbHlwaCBnbHlwaC1uYW1lPSJpY29uLWNsZWFyIiB1bmljb2RlPSImIzU5MDIwOyIgZD0iTTg5NS41OSA1NzEuMDgwaC0yNzcuNDg5djE3NS4xMjRjMCA0My43ODEtMzYuNzIxIDc5LjYwMS04MS42MTggNzkuNjAxaC00OC45NjNjLTQ0Ljg5NyAwLTgxLjYyMy0zNS44Mi04MS42MjMtNzkuNjAxdi0xNzUuMTI0aC0yNzcuNDg5Yy0xNy45NTYgMC0zMi42NDUtMTQuMzM2LTMyLjY0NS0zMS44NDZ2LTE1OS4yMDFjMC0xNy41MjEgMTQuNjg5LTMxLjg1MiAzMi42NDUtMzEuODUyaDE2LjMyM3YtMjQ1Ljk3YzAtNzAuMDU3LTE2LjMyMy0xMjguMTc0LTE2LjMyMy0xMjguMTc0IDAtMTcuNTEgMTQuNjk0LTMxLjg0NiAzMi42NDUtMzEuODQ2aDcwMS44OTFjMTcuOTYxIDAgMzIuNjQ1IDE0LjM0MSAzMi42NDUgMzEuODQ2IDAgMC0xNi4zMjMgNjAuNTA4LTE2LjMyMyAxMzEuMzU5djI0Mi43ODVoMTYuMzIzYzE3Ljk2MSAwIDMyLjY0NSAxNC4zMzYgMzIuNjQ1IDMxLjg0NnYxNTkuMjA2YzAgMTcuNTE2LTE0LjY3OSAzMS44NDYtMzIuNjQ1IDMxLjg0NnpNODg1LjExNSA0MDYuMTY0YzAtOC43NTUtNy4zNDItMTUuOTE4LTE2LjMyOC0xNS45MThoLTcxMy41NzRjLTguOTggMC0xNi4zMjMgNy4xNjgtMTYuMzIzIDE1LjkyM3YxMDYuOTIxYzAgOC43NiA3LjM0MiAxNS45MjMgMTYuMzIzIDE1LjkyM2gyOTMuODExdjIwOS4yMzljMCAzMS4wNDMgMjUuMzAzIDQ1LjQ5MSA1Ny4xMzQgNDUuNDkxaDExLjY4OWMzMS44MjYgMCA1Ny4xMjQtMTQuNDQ5IDU3LjEyNC00NS40OTF2LTIwOS4yMzRoMjkzLjgxNmM4Ljk4NiAwIDE2LjMyOC03LjE1OCAxNi4zMjgtMTUuOTIzdi0xMDYuOTI2ek02ODcuNDczIDE5Ni45MzFoLTguMTY2Yy02LjUyMyAwLTEyLjIzNy01LjU3MS0xMi4yMzctMTEuOTR2LTIwMC43MjRoLTEzMC41ODZ2MjAwLjcxOWMwIDYuMzY5LTUuNzA5IDExLjk0LTEyLjI0NyAxMS45NGgtOC4xNTZjLTYuNTIzIDAtMTIuMjM3LTUuNTcxLTEyLjIzNy0xMS45NHYtMjAwLjcyNGgtMTM4Ljc0N3YyMDAuNzI0YzAgNi4zNjktNS43MTQgMTEuOTQtMTIuMjUyIDExLjk0aC04LjE1MWMtNi41MjggMC0xMi4yNDctNS41NzEtMTIuMjQ3LTExLjk0di0yMDAuNzI0aC0xNjAuOTIyczE2LjMyOCA0Ny4wODkgMTYuMzI4IDExNy45Mzl2MjQ1Ljk3NWg2NDguMjk0di0yNDUuOTc1YzAtNzAuODUxIDE2LjMyMy0xMTcuOTM5IDE2LjMyMy0xMTcuOTM5aC0xNTIuNzU1djIwMC43MjRjMCA2LjM2OS01LjcxNCAxMS45NC0xMi4yNDIgMTEuOTR6IiAgaG9yaXotYWR2LXg9IjEwMjQiIC8+CgogICAgCiAgICA8Z2x5cGggZ2x5cGgtbmFtZT0ieHVhbnpob25nIiB1bmljb2RlPSImIzU4OTIwOyIgZD0iTTc2Ljg4MzkxMSA3NDIuODc5OTNjMCA0My4zNzE3MjMgMzUuMDg4MDc2IDc4LjUzMDQwNyA3OC41MjkzODQgNzguNTMwNDA3aDcxMC42MTIwOGM0My4zNzE3MjMgMCA3OC41MzA0MDctMzUuMDg4MDc2IDc4LjUzMDQwNy03OC41MzA0MDd2LTcxMC42MTEwNTdjMC00My4zNzE3MjMtMzUuMDg4MDc2LTc4LjUzMDQwNy03OC41MzA0MDctNzguNTMwNDA3SDE1NS40MTQzMThjLTQzLjM3MDcgMC03OC41MjkzODQgMzUuMDg4MDc2LTc4LjUyOTM4NCA3OC41MzA0MDdWNzQyLjg3OTkzaC0wLjAwMTAyM3ogbTU5LjMyOTEyOC0xNy45MTYwMDl2LTY3NC43NzY5OTJjMC0yMC41Mzk3NjEgMTYuNTkwODMtMzcuMTIwMzU4IDM3LjExOTMzNS0zNy4xMjAzNThoNjc0Ljc3ODAxNWMyMC41Mzc3MTQgMCAzNy4xMTkzMzUgMTYuNTg5ODA3IDM3LjExOTMzNSAzNy4xMjAzNTh2Njc0Ljc3Njk5MmMwIDIwLjUzODczOC0xNi41OTA4MyAzNy4xMjAzNTgtMzcuMTE5MzM1IDM3LjEyMDM1OEgxNzMuMzMxMzUxYy0yMC41Mzg3MzggMC0zNy4xMTgzMTItMTYuNTkxODU0LTM3LjExODMxMi0zNy4xMjAzNTh6TTQwMC41NjAxMzEgMTQzLjk0MzA3OUwxNzcuMjQyNDE5IDM2Ny4yMTY3ODlsNDEuMjE2NjQ1IDQxLjE3MTYxOSAyMjMuMjc2NzgtMjIzLjI3Njc4LTQxLjE3NTcxMy00MS4xNjg1NDl6TTM2My4yOTEzOTQgMTkzLjkwMDc4N2w0MzcuMzAxODY3IDQzNy4zMDU5NiA0My42MDQwMTMtNDMuNTk2ODUtNDM3LjI2NzA3NS00MzcuMzAzOTEzLTI3LjE3ODk1OCAyNy4xMzcwMDMtMTYuNDU5ODQ3IDE2LjQ1Nzh6IiAgaG9yaXotYWR2LXg9IjEwMjQiIC8+CgogICAgCiAgICA8Z2x5cGggZ2x5cGgtbmFtZT0iaWNvbi1kaXJlY3Rpb24tcmlnaHQiIHVuaWNvZGU9IiYjNTkwMDY7IiBkPSJNNTk4LjA4NiAzNjEuNDUxYzYuMDQ0IDYuMDI4IDkuMzczIDE0LjA0IDkuMzczIDIyLjU2IDAgOC41MjEtMy4zMyAxNi41MjUtOS4zNzEgMjIuNTM2TDE4Mi4wMDQgODIyLjYzYy02LjAzNCA2LjAzNC0xNC4wNTIgOS4zNTQtMjIuNTc5IDkuMzQ5LTguNTEzLTAuMDA1LTE2LjUxLTMuMzI2LTIyLjUxOS05LjM1MS02LjAyNy02LjA0Mi05LjM0Ni0xNC4wNTgtOS4zNDYtMjIuNTcxIDAtOC41MTQgMy4zMi0xNi41MjIgOS4zNDgtMjIuNTUxTDUzMC40MTcgMzg0IDEzNi45MS05LjUwNTk5OTk5OTk5OTk3MmMtNi4wMjgtNi4wMjgtOS4zNDgtMTQuMDQxLTkuMzQ4LTIyLjU2MnMzLjMyLTE2LjUzNCA5LjM0OC0yMi41NjJjNi4wMzQtNi4wMzQgMTQuMDU0LTkuMzU0IDIyLjU4LTkuMzQ4IDguNTEyIDAuMDA2IDE2LjUwOCAzLjMyNiAyMi41MTQgOS4zNDhsNDE2LjA4MiA0MTYuMDgxek00MTYuNTM3IDgwMC4wNTdjMC04LjUxNCAzLjMyLTE2LjUyMiA5LjM0OC0yMi41NTFMODE5LjM5MiAzODQgNDI1Ljg4NS05LjUwNTk5OTk5OTk5OTk3MmMtNi4wMjgtNi4wMjgtOS4zNDgtMTQuMDQxLTkuMzQ4LTIyLjU2MnMzLjMyLTE2LjUzNCA5LjM0OC0yMi41NjJjNi4wMzQtNi4wMzQgMTQuMDU0LTkuMzU0IDIyLjU4LTkuMzQ4IDguNTEyIDAuMDA2IDE2LjUwOCAzLjMyNiAyMi41MTQgOS4zNDhMODg3LjA2IDM2MS40NTJjNi4wNDQgNi4wMjggOS4zNzMgMTQuMDQgOS4zNzMgMjIuNTYgMCA4LjUyMS0zLjMzIDE2LjUyNS05LjM3MSAyMi41MzZMNDcwLjk4IDgyMi42M2MtNi4wMzQgNi4wMzQtMTQuMDUyIDkuMzU0LTIyLjU3OSA5LjM0OS04LjUxMy0wLjAwNS0xNi41MS0zLjMyNi0yMi41MTktOS4zNTEtNi4wMjYtNi4wNDItOS4zNDUtMTQuMDU4LTkuMzQ1LTIyLjU3MXoiICBob3Jpei1hZHYteD0iMTAyNCIgLz4KCiAgICAKICAgIDxnbHlwaCBnbHlwaC1uYW1lPSJpY29uLWRpcmVjdGlvbi1sZWZ0IiB1bmljb2RlPSImIzU5MDA5OyIgZD0iTTQyNS45MDkgNDA2LjU0OWMtNi4wNDQtNi4wMjgtOS4zNzMtMTQuMDQtOS4zNzMtMjIuNTYgMC04LjUyMSAzLjMzLTE2LjUyNSA5LjM3MS0yMi41MzZMODQxLjk5LTU0LjYyOTk5OTk5OTk5OTk5NWM2LjAzNC02LjAzNCAxNC4wNTItOS4zNTQgMjIuNTc5LTkuMzQ5IDguNTEzIDAuMDA1IDE2LjUxIDMuMzI2IDIyLjUxOCA5LjM1MSA2LjAyNyA2LjA0MiA5LjM0NiAxNC4wNTggOS4zNDYgMjIuNTcxIDAgOC41MTMtMy4zMiAxNi41MjItOS4zNDggMjIuNTUxTDQ5My41NzcgMzg0bDM5My41MDggMzkzLjUwN2M2LjAyOCA2LjAyOCA5LjM0OCAxNC4wNDEgOS4zNDggMjIuNTYycy0zLjMyIDE2LjUzNC05LjM0OCAyMi41NjJjLTYuMDM0IDYuMDM0LTE0LjA1NCA5LjM1NC0yMi41OCA5LjM0OC04LjUxMi0wLjAwNi0xNi41MDgtMy4zMjYtMjIuNTE0LTkuMzQ4TDQyNS45MDkgNDA2LjU0OXpNNjA3LjQ1Ny0zMi4wNTcwMDAwMDAwMDAwMTZjMCA4LjUxMy0zLjMyIDE2LjUyMi05LjM0OCAyMi41NTFMMjA0LjYwMiAzODQgNTk4LjExIDc3Ny41MDcwMDAwMDAwMDAxYzYuMDI4IDYuMDI4IDkuMzQ4IDE0LjA0MSA5LjM0OCAyMi41NjJzLTMuMzIgMTYuNTM0LTkuMzQ4IDIyLjU2MmMtNi4wMzQgNi4wMzQtMTQuMDU0IDkuMzU0LTIyLjU4IDkuMzQ4LTguNTEyLTAuMDA2LTE2LjUwOC0zLjMyNi0yMi41MTQtOS4zNDhMMTM2LjkzNCA0MDYuNTQ5Yy02LjA0NC02LjAyOC05LjM3My0xNC4wNC05LjM3My0yMi41NiAwLTguNTIxIDMuMzMtMTYuNTI1IDkuMzcxLTIyLjUzNkw1NTMuMDE1LTU0LjYyOTk5OTk5OTk5OTk5NWM2LjAzNC02LjAzNCAxNC4wNTItOS4zNTQgMjIuNTc5LTkuMzQ5IDguNTEzIDAuMDA1IDE2LjUxIDMuMzI2IDIyLjUxOCA5LjM1MSA2LjAyNyA2LjA0MiA5LjM0NSAxNC4wNTkgOS4zNDUgMjIuNTcxeiIgIGhvcml6LWFkdi14PSIxMDI0IiAvPgoKICAgIAogICAgPGdseXBoIGdseXBoLW5hbWU9InlvdTEiIHVuaWNvZGU9IiYjNTkxNDk7IiBkPSJNNzIxLjkyIDQwMS45MmwtMzg0IDM4NGMtMTAuMjQgMTAuMjQtMjUuNiAxMC4yNC0zNS44NCAwcy0xMC4yNC0yNS42IDAtMzUuODRMNjY4LjE2IDM4NCAzMDIuMDggMTcuOTJjLTEwLjI0LTEwLjI0LTEwLjI0LTI1LjYgMC0zNS44NHMyNS42LTEwLjI0IDM1Ljg0IDBsMzg0IDM4NGMxMC4yNCAxMC4yNCAxMC4yNCAyNS42IDAgMzUuODR6IiAgaG9yaXotYWR2LXg9IjEwMjQiIC8+CgogICAgCiAgICA8Z2x5cGggZ2x5cGgtbmFtZT0iamluZ2dhbyIgdW5pY29kZT0iJiM1ODk0MDsiIGQ9Ik04NDcuNTU0NTYgMjkuOTAwOEgxNzYuNDcxMDRjLTQ1LjAwNDggMC04MS4xNTcxMiAxNy42Mzg0LTk5LjIxMDI0IDQ4LjQwOTYtMTguMDU4MjQgMzAuNzY2MDgtMTUuODYxNzYgNzAuOTU4MDggNi4wNTE4NCAxMTAuMjMzNkw0MjEuOTA4NDggNzk1LjM1MTA0QzQ0My45NTAwOCA4MzQuOTEzMjggNDc2LjcyMzIgODU3LjU4OTc2IDUxMS43OTAwOCA4NTcuNTg5NzZzNjcuODA5MjgtMjIuNjQ1NzYgODkuOTEyMzItNjIuMTcyMTZsMzM4Ljk1NDI0LTYwNi45MzUwNGMyMS45MDg0OC0zOS4yNzU1MiAyNC4xNzE1Mi03OS40MzY4IDYuMDgyNTYtMTEwLjIwMjg4LTE4LjAyNzUyLTMwLjc2NjA4LTU0LjIxMDU2LTQ4LjM3ODg4LTk5LjE4NDY0LTQ4LjM3ODg4ek01MTEuODI1OTIgNzkzLjg5MTg0Yy0xMS4xNTEzNiAwLTIzLjkyMDY0LTExLjA1NDA4LTM0LjI3MzI4LTI5LjU1Nzc2TDEzOC45NTY4IDE1Ny40NjA0OGMtMTAuNjA4NjQtMTkuMDQ2NC0xMy4wOTE4NC0zNi4xNTIzMi02Ljc1MzI4LTQ2Ljk0NTI4IDYuMzQzNjgtMTAuNzY3MzYgMjIuNDg3MDQtMTYuOTQ3MiA0NC4yNzI2NC0xNi45NDcyaDY3MS4wODM1MmMyMS44MjE0NCAwIDM3LjkzOTIgNi4xNDkxMiA0NC4yNzI2NCAxNi45NDcyIDYuMzAyNzIgMTAuNzYyMjQgMy44NTUzNiAyNy44NjgxNi02Ljc4NCA0Ni45MTQ1Nkw1NDYuMDk5MiA3NjQuMzM0MDhjLTEwLjMyNzA0IDE4LjUwMzY4LTIzLjE1Nzc2IDI5LjU1Nzc2LTM0LjI3MzI4IDI5LjU1Nzc2eiBtMC4xODk0NC00NzYuNjQxMjhhMzEuODQ2NCAzMS44NDY0IDAgMCAwLTMxLjg0NjQgMzEuODUxNTJWNjM1Ljc1MDRhMzEuODYxNzYgMzEuODYxNzYgMCAwIDAgMzEuODQ2NCAzMS44NTE1MiAzMS44NjE3NiAzMS44NjE3NiAwIDAgMCAzMS44NTE1Mi0zMS44NTE1MnYtMjg2LjY1MzQ0YTMxLjg0NjQgMzEuODQ2NCAwIDAgMC0zMS44NTE1Mi0zMS44NDY0ek01MTIgMjA4LjM4OTEybS00Ny4zMzk1MiAwYTQ3LjMzOTUyIDQ3LjMzOTUyIDAgMSAxIDk0LjY3OTA0IDAgNDcuMzM5NTIgNDcuMzM5NTIgMCAxIDEtOTQuNjc5MDQgMFoiICBob3Jpei1hZHYteD0iMTAyNCIgLz4KCiAgICAKICAgIDxnbHlwaCBnbHlwaC1uYW1lPSJub3ZpZ29fbGVmdCIgdW5pY29kZT0iJiM1ODk3MDsiIGQ9Ik0yNjQuNzAwMTQ5IDM1OC4yODczODVhMjIuOTQ5Njg4IDIyLjk0OTY4OCAwIDAgMC0xNi4yODA4MDEgNi43NDM1ODhjLTguOTkxNzkyIDguOTkxNzkyLTguOTkxNzkyIDIzLjU2OTgxMiAwIDMyLjU2MTYwM2w0MjAuNzkyNzE4IDQyMC43OTM3NDJjOC45OTE3OTIgOC45OTE3OTIgMjMuNTY5ODEyIDguOTkxNzkyIDMyLjU2MTYwMyAwIDguOTkwNzY4LTguOTkxNzkyIDguOTkwNzY4LTIzLjU2OTgxMiAwLTMyLjU2MDU4bC00MjAuNzkzNzQxLTQyMC43OTQ3NjVhMjIuOTQ3NjQxIDIyLjk0NzY0MSAwIDAgMC0xNi4yNzk3NzktNi43NDM1ODh6TTY4NS40OTM4OTEtNjIuNTA2MzU2OTk5OTk5OThhMjIuOTQ5Njg4IDIyLjk0OTY4OCAwIDAgMC0xNi4yODA4MDEgNi43NDM1ODhsLTQyMC43OTM3NDIgNDIwLjc5NDc2NWMtOC45OTE3OTIgOC45OTE3OTItOC45OTE3OTIgMjMuNTY5ODEyIDAgMzIuNTYxNjA0IDguOTkwNzY4IDguOTkwNzY4IDIzLjU2OTgxMiA4Ljk5MDc2OCAzMi41NjA1OCAwbDQyMC43OTM3NDEtNDIwLjc5Mzc0MmM4Ljk5MDc2OC04Ljk5MTc5MiA4Ljk5MDc2OC0yMy41Njk4MTIgMC0zMi41NjE2MDRhMjIuOTQ4NjY1IDIyLjk0ODY2NSAwIDAgMC0xNi4yNzk3NzgtNi43NDQ2MTF6IiAgaG9yaXotYWR2LXg9IjEwMjQiIC8+CgogICAgCiAgICA8Z2x5cGggZ2x5cGgtbmFtZT0idGltZXMiIHVuaWNvZGU9IiYjNTg4ODA7IiBkPSJNOTA3LjUxIDY3LjU5NTAwMDAwMDAwMDAzbC03OS4xMDEtNzkuMTAyLTMxNi40MDYgMzE2LjQwNS0zMTYuNDA1LTMxNi40MDUtNzkuMTA0IDc5LjEwMkw0MzIuODk5IDM4NCAxMTYuNDk0IDcwMC40MDVsNzkuMTA0IDc5LjA5OSAzMTYuNDA1LTMxNi40MDVMODI4LjQxIDc3OS41MDRsNzkuMDk4LTc5LjA5OUw1OTEuMTAyIDM4NGwzMTYuNDA1LTMxNi40MDV6IiAgaG9yaXotYWR2LXg9IjEwMjUiIC8+CgogICAgCgoKICA8L2ZvbnQ+CjwvZGVmcz48L3N2Zz4K') format('svg'); /* iOS 4.1- */
    }

    .sp-iconfont {
        font-family: "sp-iconfont" !important;
        font-size: 16px;
        font-style: normal;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    .if-unselect-all:before {
        content: "\e634";
    }

    .if-clear:before {
        content: "\e68c";
    }

    .if-select-all:before {
        content: "\e628";
    }

    .if-last:before {
        content: "\e67e";
    }

    .if-first:before {
        content: "\e681";
    }

    .if-next:before {
        content: "\e70d";
    }

    .if-warning:before {
        content: "\e63c";
    }

    .if-previous:before {
        content: "\e65a";
    }

    .if-close:before {
        content: "\e600";
    }
</style>

<script>
    /**
     * @name        SelectPage
     * @desc        Simple and powerful selection plugin
     * @file        selectpage.js
     * @version     2.20
     * @author      TerryZeng
     * @contact     https://terryz.github.io/
     * @license     MIT License
     */
    ;(function ($) {
        'use strict'
        /**
         * Default options
         */
        var defaults = {
            /**
             * Data source
             * @type {string|Object}
             *
             * string：server side request url address
             * Object：JSON array，format：[{a:1,b:2,c:3},{...}]
             */
            data: undefined,
            /**
             * Language ('cn', 'en', 'ja', 'es', 'pt-br')
             * @type string
             * @default 'cn'
             */
            lang: 'cn',
            /**
             * Multiple select mode(tags)
             * @type boolean
             * @default false
             */
            multiple: false,
            /**
             * pagination or not
             * @type boolean
             * @default true
             */
            pagination: true,
            /**
             * Show up menu button
             * @type boolean
             * @default true
             */
            dropButton: true,
            /**
             * Result list visible size in pagination bar close
             * @type number
             * @default 10
             */
            listSize: 10,
            /**
             * Show control bar in multiple select mode
             * @type boolean
             * @default true
             */
            multipleControlbar: true,
            /**
             * Max selected item limited in multiple select mode
             * @type number
             * @default 0(unlimited)
             */
            maxSelectLimit: 0,
            /**
             * Select result item to close list, work on multiple select mode
             * @type boolean
             * @default false
             */
            selectToCloseList: false,
            /**
             * Init selected item key, the result will match to option.keyField option
             * @type string
             */
            initRecord: undefined,
            /**
             * The table parameter in server side mode
             * @type string
             */
            dbTable: 'tbl',
            /**
             * The value field, the value will fill to hidden element
             * @type string
             * @default 'id'
             */
            keyField: 'id',
            /**
             * The show text field, the text will show to input element or tags(multiple mode)
             * @type string
             * @default 'name'
             */
            showField: 'name',
            /**
             * Actually used to search field
             * @type string
             */
            searchField: undefined,
            /**
             * Search type ('AND' or 'OR')
             * @type string
             * @default 'AND'
             */
            andOr: 'AND',
            /**
             * Result sort type
             * @type array|boolean
             * @example
             * orderBy : ['id desc']
             */
            orderBy: false,
            /**
             * Page size
             * @type number
             * @default 10
             */
            pageSize: 10,
            /**
             * Server side request parameters
             * @type function
             * @return object
             * @example params : function(){return {'name':'aa','sex':1};}
             */
            params: undefined,
            /**
             * Custom result list item show text
             * @type function
             * @param data {object} row data
             * @return string
             */
            formatItem: undefined,
            /**
             * Have some highlight item and lost focus, auto select the highlight item
             * @type boolean
             * @default false
             */
            autoFillResult: false,
            /**
             * Auto select first item in show up result list or search result
             * depend on `autoFillResult` option set to true
             * @type boolean
             * @default false
             */
            autoSelectFirst: false,
            /**
             * Whether clear input element text when enter some keywords to search and no result return
             * @type boolean
             * @default true
             */
            noResultClean: true,
            /**
             * Select only mode
             * @type boolean
             */
            selectOnly: false,
            /**
             * Input to search delay time, work on ajax data source
             * @type number
             * @default 0.5
             */
            inputDelay: 0.5,
            /** ---------------------Callback---------------------- */
            /**
             * Result list item selected callback
             * @type function
             * @param object - selected item json data
             * @param self   - plugin object
             */
            eSelect: undefined,
            /**
             * Before result list show up callback, you can do anything prepared
             * @param self - plugin object
             */
            eOpen: undefined,
            /**
             * Server side return data convert callback
             * @type function
             * @param data {object} server side return data
             * @param self {object} plugin object
             * @return {object} return data format：
             * @example
             * {
             *   list : [{name:'aa',sex:1},{name:'bb',sex:1}...],
             *   totalRow : 100
             * }
             */
            eAjaxSuccess: undefined,
            /**
             * Close selected item tag callback (multiple mode)
             * @type function
             * @param removeCount {number} remove item count
             * @param self {object} plugin object
             */
            eTagRemove: undefined,
            /**
             * Clear selected item callback(single select mode)
             * @type function
             * @param self {object} plugin object
             */
            eClear: undefined
        }

        /**
         * SelectPage class definition
         * @constructor
         * @param {Object} input - input element
         * @param {Object} option
         */
        var SelectPage = function (input, option) {
            this.setOption(option)
            this.setLanguage()
            this.setCssClass()
            this.setProp()
            this.setElem(input)

            this.setButtonAttrDefault()
            this.setInitRecord()

            this.eDropdownButton()
            this.eInput()
            this.eWhole()
        }
        /**
         * Plugin version number
         */
        SelectPage.version = '2.20'
        /**
         * Plugin object cache key
         */
        SelectPage.dataKey = 'selectPageObject'
        /**
         * Options set
         * @param {Object} option
         */
        SelectPage.prototype.setOption = function (option) {
            // use showField by default
            option.searchField = option.searchField || option.showField

            option.andOr = option.andOr.toUpperCase()
            if (option.andOr !== 'AND' && option.andOr !== 'OR') option.andOr = 'AND'

            // support multiple field set
            var arr = ['searchField']
            for (var i = 0; i < arr.length; i++) {
                option[arr[i]] = this.strToArray(option[arr[i]])
            }

            // set multiple order field
            // example: [ ['id ASC'], ['name DESC'] ]
            if (option.orderBy !== false)
                option.orderBy = this.setOrderbyOption(option.orderBy, option.showField)
            //close auto fill result and auto select first in multiple mode and select item not close list
            if (option.multiple && !option.selectToCloseList) {
                option.autoFillResult = false
                option.autoSelectFirst = false
            }
            // show all item when pagination bar close, limited 200
            if (!option.pagination) option.pageSize = 200
            if ($.type(option.listSize) !== 'number' || option.listSize < 0)
                option.listSize = 10

            this.option = option
        }

        /**
         * String convert to array
         * @param str {string}
         * @return {Array}
         */
        SelectPage.prototype.strToArray = function (str) {
            return str ? str.replace(/[\s　]+/g, '').split(',') : ''
        }

        /**
         * Set order field
         * @param {Array} arg_order
         * @param {string} arg_field - default sort field
         * @return {Array}
         */
        SelectPage.prototype.setOrderbyOption = function (arg_order, arg_field) {
            var arr = [],
                orders = []
            if (typeof arg_order === 'object') {
                for (var i = 0; i < arg_order.length; i++) {
                    orders = $.trim(arg_order[i]).split(' ')
                    if (orders.length) {
                        arr.push(orders.length === 2 ? orders.concat() : [orders[0], 'ASC'])
                    }
                }
            } else {
                orders = $.trim(arg_order).split(' ')
                arr[0] = orders.length === 2
                    ? orders.concat()
                    : orders[0].toUpperCase().match(/^(ASC|DESC)$/i)
                        ? [arg_field, orders[0].toUpperCase()]
                        : [orders[0], 'ASC']
            }
            return arr
        }

        /**
         * i18n
         */
        SelectPage.prototype.setLanguage = function () {
            var message, p = this.option
            switch (p.lang) {
                // German
                case 'de':
                    message = {
                        add_btn: 'Hinzufügen-Button',
                        add_title: 'Box hinzufügen',
                        del_btn: 'Löschen-Button',
                        del_title: 'Box löschen',
                        next: 'Nächsten',
                        next_title: 'Nächsten' + p.pageSize + ' (Pfeil-rechts)',
                        prev: 'Vorherigen',
                        prev_title: 'Vorherigen' + p.pageSize + ' (Pfeil-links)',
                        first_title: 'Ersten (Umschalt + Pfeil-links)',
                        last_title: 'Letzten (Umschalt + Pfeil-rechts)',
                        get_all_btn: 'alle (Pfeil-runter)',
                        get_all_alt: '(Button)',
                        close_btn: 'Schließen (Tab)',
                        close_alt: '(Button)',
                        loading: 'lade...',
                        loading_alt: '(lade)',
                        page_info: 'page_num von page_count',
                        select_ng: 'Achtung: Bitte wählen Sie aus der Liste aus.',
                        select_ok: 'OK : Richtig ausgewählt.',
                        not_found: 'nicht gefunden',
                        ajax_error:
                            'Bei der Verbindung zum Server ist ein Fehler aufgetreten.',
                        clear: 'Löschen Sie den Inhalt',
                        select_all: 'Wähle diese Seite',
                        unselect_all: 'Diese Seite entfernen',
                        clear_all: 'Alles löschen',
                        max_selected:
                            'Sie können nur bis zu max_selected_limit Elemente auswählen'
                    }
                    break

                // English
                case 'en':
                    message = {
                        add_btn: 'Add button',
                        add_title: 'add a box',
                        del_btn: 'Del button',
                        del_title: 'delete a box',
                        next: 'Next',
                        next_title: 'Next' + p.pageSize + ' (Right key)',
                        prev: 'Prev',
                        prev_title: 'Prev' + p.pageSize + ' (Left key)',
                        first_title: 'First (Shift + Left key)',
                        last_title: 'Last (Shift + Right key)',
                        get_all_btn: 'Get All (Down key)',
                        get_all_alt: '(button)',
                        close_btn: 'Close (Tab key)',
                        close_alt: '(button)',
                        loading: 'loading...',
                        loading_alt: '(loading)',
                        page_info: 'Page page_num of page_count',
                        select_ng: 'Attention : Please choose from among the list.',
                        select_ok: 'OK : Correctly selected.',
                        not_found: 'not found',
                        ajax_error: 'An error occurred while connecting to server.',
                        clear: 'Clear content',
                        select_all: 'Select current page',
                        unselect_all: 'Clear current page',
                        clear_all: 'Clear all selected',
                        max_selected: 'You can only select up to max_selected_limit items'
                    }
                    break

                // Spanish
                case 'es':
                    message = {
                        add_btn: 'Agregar boton',
                        add_title: 'Agregar una opcion',
                        del_btn: 'Borrar boton',
                        del_title: 'Borrar una opcion',
                        next: 'Siguiente',
                        next_title: 'Proximas ' + p.pageSize + ' (tecla derecha)',
                        prev: 'Anterior',
                        prev_title: 'Anteriores ' + p.pageSize + ' (tecla izquierda)',
                        first_title: 'Primera (Shift + Left)',
                        last_title: 'Ultima (Shift + Right)',
                        get_all_btn: 'Ver todos (tecla abajo)',
                        get_all_alt: '(boton)',
                        close_btn: 'Cerrar (tecla TAB)',
                        close_alt: '(boton)',
                        loading: 'Cargando...',
                        loading_alt: '(Cargando)',
                        page_info: 'page_num de page_count',
                        select_ng: 'Atencion: Elija una opcion de la lista.',
                        select_ok: 'OK: Correctamente seleccionado.',
                        not_found: 'no encuentre',
                        ajax_error: 'Un error ocurrió mientras conectando al servidor.',
                        clear: 'Borrar el contenido',
                        select_all: 'Elija esta página',
                        unselect_all: 'Borrar esta página',
                        clear_all: 'Borrar todo marcado',
                        max_selected:
                            'Solo puedes seleccionar hasta max_selected_limit elementos'
                    }
                    break

                // Brazilian Portuguese
                case 'pt-br':
                    message = {
                        add_btn: 'Adicionar botão',
                        add_title: 'Adicionar uma caixa',
                        del_btn: 'Apagar botão',
                        del_title: 'Apagar uma caixa',
                        next: 'Próxima',
                        next_title: 'Próxima ' + p.pageSize + ' (tecla direita)',
                        prev: 'Anterior',
                        prev_title: 'Anterior ' + p.pageSize + ' (tecla esquerda)',
                        first_title: 'Primeira (Shift + Left)',
                        last_title: 'Última (Shift + Right)',
                        get_all_btn: 'Ver todos (Seta para baixo)',
                        get_all_alt: '(botão)',
                        close_btn: 'Fechar (tecla TAB)',
                        close_alt: '(botão)',
                        loading: 'Carregando...',
                        loading_alt: '(Carregando)',
                        page_info: 'page_num de page_count',
                        select_ng: 'Atenção: Escolha uma opção da lista.',
                        select_ok: 'OK: Selecionado Corretamente.',
                        not_found: 'não encontrado',
                        ajax_error: 'Um erro aconteceu enquanto conectando a servidor.',
                        clear: 'Limpe o conteúdo',
                        select_all: 'Selecione a página atual',
                        unselect_all: 'Remova a página atual',
                        clear_all: 'Limpar tudo',
                        max_selected: 'Você só pode selecionar até max_selected_limit itens'
                    }
                    break

                // Japanese
                case 'ja':
                    message = {
                        add_btn: '追加ボタン',
                        add_title: '入力ボックスを追加します',
                        del_btn: '削除ボタン',
                        del_title: '入力ボックスを削除します',
                        next: '次へ',
                        next_title: '次の' + p.pageSize + '件 (右キー)',
                        prev: '前へ',
                        prev_title: '前の' + p.pageSize + '件 (左キー)',
                        first_title: '最初のページへ (Shift + 左キー)',
                        last_title: '最後のページへ (Shift + 右キー)',
                        get_all_btn: '全件取得 (下キー)',
                        get_all_alt: '画像:ボタン',
                        close_btn: '閉じる (Tabキー)',
                        close_alt: '画像:ボタン',
                        loading: '読み込み中...',
                        loading_alt: '画像:読み込み中...',
                        page_info: 'page_num 件 (全 page_count 件)',
                        select_ng: '注意 : リストの中から選択してください',
                        select_ok: 'OK : 正しく選択されました。',
                        not_found: '(0 件)',
                        ajax_error: 'サーバとの通信でエラーが発生しました。',
                        clear: 'コンテンツをクリアする',
                        select_all: '当ページを選びます',
                        unselect_all: '移して当ページを割ります',
                        clear_all: '選択した項目をクリアする',
                        max_selected:
                            '最多で max_selected_limit のプロジェクトを選ぶことしかできません'
                    }
                    break
                // 中文
                case 'cn':
                default:
                    message = {
                        add_btn: '添加按钮',
                        add_title: '添加区域',
                        del_btn: '删除按钮',
                        del_title: '删除区域',
                        next: '下一页',
                        next_title: '下' + p.pageSize + ' (→)',
                        prev: '上一页',
                        prev_title: '上' + p.pageSize + ' (←)',
                        first_title: '首页 (Shift + ←)',
                        last_title: '尾页 (Shift + →)',
                        get_all_btn: '获得全部 (↓)',
                        get_all_alt: '(按钮)',
                        close_btn: '关闭 (Tab键)',
                        close_alt: '(按钮)',
                        loading: '读取中...',
                        loading_alt: '(读取中)',
                        page_info: '第 page_num 页(共page_count页)',
                        select_ng: '请注意：请从列表中选择.',
                        select_ok: 'OK : 已经选择.',
                        not_found: '无查询结果',
                        ajax_error: '连接到服务器时发生错误！',
                        clear: '清除内容',
                        select_all: '选择当前页项目',
                        unselect_all: '取消选择当前页项目',
                        clear_all: '清除全部已选择项目',
                        max_selected: '最多只能选择 max_selected_limit 个项目'
                    }
                    break
            }
            this.message = message
        }

        /**
         * Css classname defined
         */
        SelectPage.prototype.setCssClass = function () {
            var css_class = {
                container: 'sp_container',
                container_open: 'sp_container_open',
                re_area: 'sp_result_area',
                result_open: 'sp_result_area_open',
                control_box: 'sp_control_box',
                //multiple select mode
                element_box: 'sp_element_box',
                navi: 'sp_navi',
                //result list
                results: 'sp_results',
                re_off: 'sp_results_off',
                select: 'sp_over',
                select_ok: 'sp_select_ok',
                select_ng: 'sp_select_ng',
                selected: 'sp_selected',
                input_off: 'sp_input_off',
                message_box: 'sp_message_box',
                disabled: 'sp_disabled',

                button: 'sp_button',
                caret_open: 'sp_caret_open',
                btn_on: 'sp_btn_on',
                btn_out: 'sp_btn_out',
                input: 'sp_input',
                clear_btn: 'sp_clear_btn',
                align_right: 'sp_align_right'
            }
            this.css_class = css_class
        }

        /**
         * Plugin inner properties
         */
        SelectPage.prototype.setProp = function () {
            this.prop = {
                //input disabled status
                disabled: false,
                current_page: 1,
                //total page
                max_page: 1,
                //ajax data loading status
                is_loading: false,
                xhr: false,
                key_paging: false,
                key_select: false,
                //last selected item value
                prev_value: '',
                //last selected item text
                selected_text: '',
                last_input_time: undefined,
                init_set: false
            }
            this.template = {
                tag: {
                    content:
                        '<li class="selected_tag" itemvalue="#item_value#">#item_text#<span class="tag_close"><i class="sp-iconfont if-close"></i></span></li>',
                    textKey: '#item_text#',
                    valueKey: '#item_value#'
                },
                page: {
                    current: 'page_num',
                    total: 'page_count'
                },
                msg: {
                    maxSelectLimit: 'max_selected_limit'
                }
            }
        }

        /**
         * Get the actual width/height of invisible DOM elements with jQuery.
         * Source code come from dreamerslab/jquery.actual
         * @param element
         * @param method
         * @returns {*}
         */
        SelectPage.prototype.elementRealSize = function (element, method) {
            var defaults = {
                absolute: false,
                clone: false,
                includeMargin: false,
                display: 'block'
            }
            var configs = defaults,
                $target = element.eq(0),
                fix,
                restore,
                tmp = [],
                style = '',
                $hidden

            fix = function () {
                // get all hidden parents
                $hidden = $target.parents().addBack().filter(':hidden')
                style +=
                    'visibility: hidden !important; display: ' +
                    configs.display +
                    ' !important; '

                if (configs.absolute === true) style += 'position: absolute !important;'

                // save the origin style props
                // set the hidden el css to be got the actual value later
                $hidden.each(function () {
                    // Save original style. If no style was set, attr() returns undefined
                    var $this = $(this), thisStyle = $this.attr('style')
                    tmp.push(thisStyle)
                    // Retain as much of the original style as possible, if there is one
                    $this.attr('style', thisStyle ? thisStyle + ';' + style : style)
                })
            }

            restore = function () {
                // restore origin style values
                $hidden.each(function (i) {
                    var $this = $(this),
                        _tmp = tmp[i]

                    if (_tmp === undefined) $this.removeAttr('style')
                    else $this.attr('style', _tmp)
                })
            }

            fix()
            // get the actual value with user specific methed
            // it can be 'width', 'height', 'outerWidth', 'innerWidth'... etc
            // configs.includeMargin only works for 'outerWidth' and 'outerHeight'
            var actual = /(outer)/.test(method)
                ? $target[method](configs.includeMargin)
                : $target[method]()

            restore()
            // IMPORTANT, this plugin only return the value of the first element
            return actual
        }

        /**
         * Dom building
         * @param {Object} combo_input - original input element
         */
        SelectPage.prototype.setElem = function (combo_input) {
            // 1. build Dom object
            var elem = {},
                p = this.option,
                css = this.css_class,
                msg = this.message,
                input = $(combo_input)
            var orgWidth = input.outerWidth()
            // fix input width in hidden situation
            if (orgWidth <= 0) orgWidth = this.elementRealSize(input, 'outerWidth')
            if (orgWidth < 150) orgWidth = 150

            elem.combo_input = input
                .attr({autocomplete: 'off'})
                .addClass(css.input)
                .wrap('<div>')
            if (p.selectOnly) elem.combo_input.prop('readonly', true)
            elem.container = elem.combo_input.parent().addClass(css.container)
            if (elem.combo_input.prop('disabled')) {
                if (p.multiple) elem.container.addClass(css.disabled)
                else elem.combo_input.addClass(css.input_off)
            }

            // set outer box width
            elem.container.width(orgWidth)

            elem.button = $('<div>').addClass(css.button)
            // drop down button
            elem.dropdown = $('<span class="sp_caret"></span>')
            // clear button 'X' in single mode
            elem.clear_btn = $('<div>')
                .html($('<i>').addClass('sp-iconfont if-close'))
                .addClass(css.clear_btn)
                .attr('title', msg.clear)
            if (!p.dropButton) elem.clear_btn.addClass(css.align_right)

            // main box in multiple mode
            elem.element_box = $('<ul>').addClass(css.element_box)
            if (p.multiple && p.multipleControlbar)
                elem.control = $('<div>').addClass(css.control_box)
            // result list box
            elem.result_area = $('<div>').addClass(css.re_area)
            // pagination bar
            if (p.pagination)
                elem.navi = $('<div>').addClass('sp_pagination').append('<ul>')
            elem.results = $('<ul>').addClass(css.results)

            var namePrefix = '_text',
                input_id = elem.combo_input.attr('id') || elem.combo_input.attr('name'),
                input_name = elem.combo_input.attr('name') || 'selectPage',
                hidden_name = input_name,
                hidden_id = input_id

            // switch the id and name attributes of input/hidden element
            elem.hidden = $('<input type="hidden" class="sp_hidden" />')
                .attr({
                    name: hidden_name,
                    id: hidden_id
                })
                .val('')
            elem.combo_input.attr({
                name: input_name + namePrefix,
                id: input_id + namePrefix
            })

            // 2. DOM element put
            elem.container.append(elem.hidden)
            if (p.dropButton) {
                elem.container.append(elem.button)
                elem.button.append(elem.dropdown)
            }
            $(document.body).append(elem.result_area)
            elem.result_area.append(elem.results)
            if (p.pagination) elem.result_area.append(elem.navi)

            //Multiple select mode
            if (p.multiple) {
                if (p.multipleControlbar) {
                    elem.control.append(
                        '<button type="button" class="btn btn-default sp_clear_all" ><i class="sp-iconfont if-clear"></i></button>'
                    )
                    elem.control.append(
                        '<button type="button" class="btn btn-default sp_unselect_all" ><i class="sp-iconfont if-unselect-all"></i></button>'
                    )
                    elem.control.append(
                        '<button type="button" class="btn btn-default sp_select_all" ><i class="sp-iconfont if-select-all"></i></button>'
                    )
                    elem.control_text = $('<p>')
                    elem.control.append(elem.control_text)
                    elem.result_area.prepend(elem.control)
                }
                elem.container.addClass('sp_container_combo')
                elem.combo_input.addClass('sp_combo_input').before(elem.element_box)
                var li = $('<li>').addClass('input_box')
                li.append(elem.combo_input)
                elem.element_box.append(li)
                if (elem.combo_input.attr('placeholder'))
                    elem.combo_input.attr(
                        'placeholder_bak',
                        elem.combo_input.attr('placeholder')
                    )
            }

            this.elem = elem
        }

        /**
         * Drop down button set to default
         */
        SelectPage.prototype.setButtonAttrDefault = function () {
            /*
            if (this.option.selectOnly) {
              if ($(this.elem.combo_input).val() !== '') {
                if ($(this.elem.hidden).val() !== '') {
                  //选择条件
                  $(this.elem.combo_input).attr('title', this.message.select_ok).removeClass(this.css_class.select_ng).addClass(this.css_class.select_ok);
                } else {
                  //输入方式
                  $(this.elem.combo_input).attr('title', this.message.select_ng).removeClass(this.css_class.select_ok).addClass(this.css_class.select_ng);
                }
              } else {
                $(this.elem.hidden).val('');
                $(this.elem.combo_input).removeAttr('title').removeClass(this.css_class.select_ng);
              }
            }
            */
            //this.elem.button.attr('title', this.message.get_all_btn);
            if (this.option.dropButton)
                this.elem.button.attr('title', this.message.close_btn)
        }

        /**
         * Set item need selected after init
         * set selected item ways:
         * <input value="key">
         * <input data-init="key">
         */
        SelectPage.prototype.setInitRecord = function (refresh) {
            var self = this, p = self.option, el = self.elem, key = ''
            if ($.type(el.combo_input.data('init')) != 'undefined') {
                p.initRecord = String(el.combo_input.data('init'))
            }

            //data-init and value attribute can be init plugin selected item
            //but, if set data-init and value attribute in the same time, plugin will choose data-init attribute first
            if (!refresh && !p.initRecord && el.combo_input.val()) {
                p.initRecord = el.combo_input.val()
            }

            el.combo_input.val('')

            if (!refresh) {
                el.hidden.val(p.initRecord)
            }

            key = refresh && el.hidden.val() ? el.hidden.val() : p.initRecord

            if (key) {
                if (typeof p.data === 'object') {
                    var data = new Array()
                    var keyarr = key.split(',')
                    $.each(keyarr, function (index, row) {
                        for (var i = 0; i < p.data.length; i++) {
                            if (p.data[i][p.keyField] == row) {
                                data.push(p.data[i])
                                break
                            }
                        }
                    })
                    if (!p.multiple && data.length > 1) data = [data[0]]
                    self.afterInit(self, data)
                } else {
                    //ajax data source mode to init selected item
                    $.ajax({
                        dataType: 'json',
                        type: 'POST',
                        url: p.data,
                        data: {
                            searchTable: p.dbTable,
                            searchKey: p.keyField,
                            searchValue: key
                        },
                        success: function (json) {
                            var d = null
                            if (p.eAjaxSuccess && $.isFunction(p.eAjaxSuccess))
                                d = p.eAjaxSuccess(json)
                            self.afterInit(self, d.list)
                        },
                        error: function () {
                            self.ajaxErrorNotify(self)
                        }
                    })
                }
            }
        }

        /**
         * Selected item set to plugin
         * @param {Object} self
         * @param {Object} data - selected item data
         */
        SelectPage.prototype.afterInit = function (self, data) {
            if (!data || ($.isArray(data) && data.length === 0)) return
            if (!$.isArray(data)) data = [data]
            var p = self.option,
                css = self.css_class

            var getText = function (row) {
                var text = row[p.showField]
                if (p.formatItem && $.isFunction(p.formatItem)) {
                    try {
                        text = p.formatItem(row)
                    } catch (e) {
                    }
                }
                return text
            }

            if (p.multiple) {
                self.prop.init_set = true
                self.clearAll(self)
                $.each(data, function (i, row) {
                    var item = {text: getText(row), value: row[p.keyField]}
                    if (!self.isAlreadySelected(self, item)) self.addNewTag(self, row, item)
                })
                self.tagValuesSet(self)
                self.inputResize(self)
                self.prop.init_set = false
            } else {
                var row = data[0]
                self.elem.combo_input.val(getText(row))
                self.elem.hidden.val(row[p.keyField])
                self.prop.prev_value = getText(row)
                self.prop.selected_text = getText(row)
                if (p.selectOnly) {
                    self.elem.combo_input
                        .attr('title', self.message.select_ok)
                        .removeClass(css.select_ng)
                        .addClass(css.select_ok)
                }
                self.putClearButton()
            }
        }

        /**
         * Drop down button event bind
         */
        SelectPage.prototype.eDropdownButton = function () {
            var self = this
            if (self.option.dropButton) {
                self.elem.button.mouseup(function (ev) {
                    ev.stopPropagation()
                    if (
                        self.elem.result_area.is(':hidden') &&
                        !self.elem.combo_input.prop('disabled')
                    ) {
                        self.elem.combo_input.focus()
                    } else self.hideResults(self)
                })
            }
        }

        /**
         * Events bind
         */
        SelectPage.prototype.eInput = function () {
            var self = this, p = self.option, el = self.elem, msg = self.message
            var showList = function () {
                self.prop.page_move = false
                self.suggest(self)
                self.setCssFocusedInput(self)
            }
            el.combo_input
                .keyup(function (e) {
                    self.processKey(self, e)
                })
                .keydown(function (e) {
                    self.processControl(self, e)
                })
                .focus(function (e) {
                    // When focus on input, show the result list
                    if (el.result_area.is(':hidden')) {
                        e.stopPropagation()
                        self.prop.first_show = true
                        showList()
                    }
                })
            el.container.on(
                'click.SelectPage',
                'div.' + self.css_class.clear_btn,
                function (e) {
                    e.stopPropagation()
                    if (!self.disabled(self)) {
                        self.clearAll(self, true)
                        if (p.eClear && $.isFunction(p.eClear)) p.eClear(self)
                    }
                }
            )
            el.result_area.on('mousedown.SelectPage', function (e) {
                e.stopPropagation()
            })
            if (p.multiple) {
                if (p.multipleControlbar) {
                    // Select all item of current page
                    el.control
                        .find('.sp_select_all')
                        .on('click.SelectPage', function () {
                            self.selectAllLine(self)
                        })
                        .hover(
                            function () {
                                el.control_text.html(msg.select_all)
                            },
                            function () {
                                el.control_text.html('')
                            }
                        )
                    // Cancel select all item of current page
                    el.control
                        .find('.sp_unselect_all')
                        .on('click.SelectPage', function () {
                            self.unSelectAllLine(self)
                        })
                        .hover(
                            function () {
                                el.control_text.html(msg.unselect_all)
                            },
                            function () {
                                el.control_text.html('')
                            }
                        )
                    // Clear all selected item
                    el.control
                        .find('.sp_clear_all')
                        .on('click.SelectPage', function () {
                            self.clearAll(self, true)
                        })
                        .hover(
                            function () {
                                el.control_text.html(msg.clear_all)
                            },
                            function () {
                                el.control_text.html('')
                            }
                        )
                }
                el.element_box.on('click.SelectPage', function (e) {
                    var srcEl = e.target || e.srcElement
                    if ($(srcEl).is('ul')) el.combo_input.focus()
                })
                // Tag close
                el.element_box.on('click.SelectPage', 'span.tag_close', function () {
                    var li = $(this).closest('li'), data = li.data('dataObj')
                    self.removeTag(self, li)
                    showList()
                    if (p.eTagRemove && $.isFunction(p.eTagRemove)) p.eTagRemove([data])
                })
                self.inputResize(self)
            }
        }

        /**
         * Out of plugin area click event handler
         */
        SelectPage.prototype.eWhole = function () {
            var self = this,
                css = self.css_class
            var cleanContent = function (obj) {
                obj.elem.combo_input.val('')
                if (!obj.option.multiple) obj.elem.hidden.val('')
                obj.prop.selected_text = ''
            }

            // Out of plugin area
            $(document.body)
                .off('mousedown.selectPage')
                .on('mousedown.selectPage', function (e) {
                    var ele = e.target || e.srcElement
                    var sp = $(ele).closest('div.' + css.container)
                    // Open status result list
                    $('div.' + css.container + '.' + css.container_open).each(function () {
                        if (this == sp[0]) return
                        var $this = $(this),
                            d = $this.find('input.' + css.input).data(SelectPage.dataKey)

                        if (
                            !d.elem.combo_input.val() &&
                            d.elem.hidden.val() &&
                            !d.option.multiple
                        ) {
                            d.prop.current_page = 1 // reset page to 1
                            cleanContent(d)
                            d.hideResults(d)
                            return true
                        }
                        if (d.elem.results.find('li').not('.' + css.message_box).length) {
                            if (d.option.autoFillResult) {
                                // have selected item, then hide result list
                                if (d.elem.hidden.val()) d.hideResults(d)
                                else if (d.elem.results.find('li.sp_over').length) {
                                    // no one selected and have highlight item, select the highlight item
                                    d.selectCurrentLine(d)
                                } else if (d.option.autoSelectFirst) {
                                    // no one selected, no one highlight, select the first item
                                    d.nextLine(d)
                                    d.selectCurrentLine(d)
                                } else d.hideResults(d)
                            } else d.hideResults(d)
                        } else {
                            // when no one item match, clear search keywords
                            if (d.option.noResultClean) cleanContent(d)
                            else {
                                if (!d.option.multiple) d.elem.hidden.val('')
                            }
                            d.hideResults(d)
                        }
                    })
                })
        }

        /**
         * Result list event bind
         */
        SelectPage.prototype.eResultList = function () {
            var self = this,
                css = this.css_class
            self.elem.results
                .children('li')
                .hover(
                    function () {
                        if (self.prop.key_select) {
                            self.prop.key_select = false
                            return
                        }
                        if (
                            !$(this).hasClass(css.selected) &&
                            !$(this).hasClass(css.message_box)
                        ) {
                            $(this).addClass(css.select)
                            self.setCssFocusedResults(self)
                        }
                    },
                    function () {
                        $(this).removeClass(css.select)
                    }
                )
                .click(function (e) {
                    if (self.prop.key_select) {
                        self.prop.key_select = false
                        return
                    }
                    e.preventDefault()
                    e.stopPropagation()

                    if (!$(this).hasClass(css.selected)) self.selectCurrentLine(self)
                })
        }

        /**
         * Reposition result list when list beyond the visible area
         */
        SelectPage.prototype.eScroll = function () {
            var css = this.css_class
            $(window).on('scroll.SelectPage', function () {
                $('div.' + css.container + '.' + css.container_open).each(function () {
                    var $this = $(this),
                        d = $this.find('input.' + css.input).data(SelectPage.dataKey),
                        offset = d.elem.result_area.offset(),
                        screenScrollTop = $(window).scrollTop(),
                        docHeight = $(document).height(),
                        viewHeight = $(window).height(),
                        listHeight = d.elem.result_area.outerHeight(),
                        listBottom = offset.top + listHeight,
                        hasOverflow = docHeight > viewHeight,
                        down = d.elem.result_area.hasClass('shadowDown')
                    if (hasOverflow) {
                        if (down) {
                            // open down
                            if (listBottom > viewHeight + screenScrollTop) d.calcResultsSize(d)
                        } else {
                            // open up
                            if (offset.top < screenScrollTop) d.calcResultsSize(d)
                        }
                    }
                })
            })
        }

        /**
         * Page bar button event bind
         */
        SelectPage.prototype.ePaging = function () {
            var self = this
            if (!self.option.pagination) return
            self.elem.navi
                .find('li.csFirstPage')
                .off('click')
                .on('click', function (ev) {
                    //$(self.elem.combo_input).focus();
                    ev.preventDefault()
                    self.firstPage(self)
                })

            self.elem.navi
                .find('li.csPreviousPage')
                .off('click')
                .on('click', function (ev) {
                    //$(self.elem.combo_input).focus();
                    ev.preventDefault()
                    self.prevPage(self)
                })

            self.elem.navi
                .find('li.csNextPage')
                .off('click')
                .on('click', function (ev) {
                    //$(self.elem.combo_input).focus();
                    ev.preventDefault()
                    self.nextPage(self)
                })

            self.elem.navi
                .find('li.csLastPage')
                .off('click')
                .on('click', function (ev) {
                    //$(self.elem.combo_input).focus();
                    ev.preventDefault()
                    self.lastPage(self)
                })
        }

        /**
         * Ajax request fail
         * @param {Object} self
         */
        SelectPage.prototype.ajaxErrorNotify = function (self) {
            self.showMessage(self.message.ajax_error)
        }

        /**
         * Message box
         * @param {Object} self
         * @param msg {string} the text need to show
         */
        SelectPage.prototype.showMessage = function (self, msg) {
            if (!msg) return
            var msgLi =
                '<li class="' +
                self.css_class.message_box +
                '"><i class="sp-iconfont if-warning"></i> ' +
                msg +
                '</li>'
            self.elem.results.empty().append(msgLi).show()
            self.calcResultsSize(self)
            self.setOpenStatus(self, true)
            self.elem.control.hide()
            if (self.option.pagination) self.elem.navi.hide()
        }

        /**
         * @desc Scroll
         * @param {Object} self
         * @param {boolean} enforce
         */
        SelectPage.prototype.scrollWindow = function (self, enforce) {
            var current_result = self.getCurrentLine(self)
            var target_size
            var target_top = current_result && !enforce
                ? current_result.offset().top
                : self.elem.container.offset().top

            self.prop.size_li = self.elem.results.children('li:first').outerHeight()
            target_size = self.prop.size_li

            var gap
            var client_height = $(window).height()
            var scroll_top = $(window).scrollTop()
            var scroll_bottom = scroll_top + client_height - target_size

            if (current_result.length) {
                if (target_top < scroll_top || target_size > client_height) {
                    //scroll to top
                    gap = target_top - scroll_top
                } else if (target_top > scroll_bottom) {
                    //scroll down
                    gap = target_top - scroll_bottom
                } else return //do not scroll
            } else if (target_top < scroll_top) gap = target_top - scroll_top
            window.scrollBy(0, gap)
        }

        /**
         * change css class by status
         * @param self
         * @param status {boolean} true: open, false: close
         */
        SelectPage.prototype.setOpenStatus = function (self, status) {
            var el = self.elem,
                css = self.css_class
            if (status) {
                el.container.addClass(css.container_open)
                el.result_area.addClass(css.result_open)
            } else {
                el.container.removeClass(css.container_open)
                el.result_area.removeClass(css.result_open)
            }
        }

        /**
         * input element in focus css class set
         * @param {Object} self
         */
        SelectPage.prototype.setCssFocusedInput = function (self) {
            //$(self.elem.results).addClass(self.css_class.re_off);
            //$(self.elem.combo_input).removeClass(self.css_class.input_off);
        }

        /**
         * set result list get focus and input element lost focus
         * @param {Object} self
         */
        SelectPage.prototype.setCssFocusedResults = function (self) {
            //$(self.elem.results).removeClass(self.css_class.re_off);
            //$(self.elem.combo_input).addClass(self.css_class.input_off);
        }

        /**
         * Quick search input keywords listener
         * @param {Object} self
         */
        SelectPage.prototype.checkValue = function (self) {
            var now_value = self.elem.combo_input.val()
            if (now_value != self.prop.prev_value) {
                self.prop.prev_value = now_value
                self.prop.first_show = false

                if (self.option.selectOnly) self.setButtonAttrDefault()
                if (!self.option.multiple && !now_value) {
                    self.elem.combo_input.val('')
                    self.elem.hidden.val('')
                    self.elem.clear_btn.remove()
                }

                self.suggest(self)
            }
        }

        /**
         * Input handle（regular input）
         * @param {Object} self
         * @param {Object} e - event object
         */
        SelectPage.prototype.processKey = function (self, e) {
            if ($.inArray(e.keyCode, [37, 38, 39, 40, 27, 9, 13]) === -1) {
                if (e.keyCode != 16) self.setCssFocusedInput(self) // except Shift(16)
                self.inputResize(self)
                if ($.type(self.option.data) === 'string') {
                    self.prop.last_input_time = e.timeStamp
                    setTimeout(function () {
                        if (e.timeStamp - self.prop.last_input_time === 0)
                            self.checkValue(self)
                    }, self.option.inputDelay * 1000)
                } else {
                    self.checkValue(self)
                }
            }
        }

        /**
         * Input handle（control key）
         * @param {Object} self
         * @param {Object} e - event object
         */
        SelectPage.prototype.processControl = function (self, e) {
            if (
                ($.inArray(e.keyCode, [37, 38, 39, 40, 27, 9]) > -1 &&
                    self.elem.result_area.is(':visible')) ||
                ($.inArray(e.keyCode, [13, 9]) > -1 && self.getCurrentLine(self))
            ) {
                e.preventDefault()
                e.stopPropagation()
                e.cancelBubble = true
                e.returnValue = false
                switch (e.keyCode) {
                    case 37: // left
                        if (e.shiftKey) self.firstPage(self)
                        else self.prevPage(self)
                        break
                    case 38: // up
                        self.prop.key_select = true
                        self.prevLine(self)
                        break
                    case 39: // right
                        if (e.shiftKey) self.lastPage(self)
                        else self.nextPage(self)
                        break
                    case 40: // down
                        if (self.elem.results.children('li').length) {
                            self.prop.key_select = true
                            self.nextLine(self)
                        } else self.suggest(self)
                        break
                    case 9: // tab
                        self.prop.key_paging = true
                        self.selectCurrentLine(self)
                        //self.hideResults(self);
                        break
                    case 13: // return
                        self.selectCurrentLine(self)
                        break
                    case 27: //  escape
                        self.prop.key_paging = true
                        self.hideResults(self)
                        break
                }
            }
        }

        /**
         * Abort Ajax request
         * @param {Object} self
         */
        SelectPage.prototype.abortAjax = function (self) {
            if (self.prop.xhr) {
                self.prop.xhr.abort()
                self.prop.xhr = false
            }
        }

        /**
         * Suggest result of search keywords
         * @param {Object} self
         */
        SelectPage.prototype.suggest = function (self) {
            var q_word,
                val = $.trim(self.elem.combo_input.val())
            if (self.option.multiple) q_word = val
            else {
                if (val && val === self.prop.selected_text) q_word = ''
                else q_word = val
            }
            q_word = q_word.split(/[\s　]+/)

            //Before show up result list callback
            if (self.option.eOpen && $.isFunction(self.option.eOpen))
                self.option.eOpen.call(self)

            self.abortAjax(self)
            //self.setLoading(self);
            var which_page_num = self.prop.current_page || 1

            if (typeof self.option.data == 'object')
                self.searchForJson(self, q_word, which_page_num)
            else self.searchForDb(self, q_word, which_page_num)
        }

        /**
         * Loading
         * @param {Object} self
         */
        SelectPage.prototype.setLoading = function (self) {
            if (self.elem.results.html() === '') {
                //self.calcResultsSize(self);
                self.setOpenStatus(self, true)
            }
        }

        /**
         * Search for ajax
         * @param {Object} self
         * @param {Array} q_word - query keyword
         * @param {number} which_page_num - target page number
         */
        SelectPage.prototype.searchForDb = function (self, q_word, which_page_num) {
            var p = self.option
            if (!p.eAjaxSuccess || !$.isFunction(p.eAjaxSuccess)) self.hideResults(self)
            var _paramsFunc = p.params,
                _params = {},
                searchKey = p.searchField
            //when have new query keyword, then reset page number to 1.
            if (q_word.length && q_word[0] && q_word[0] !== self.prop.prev_value)
                which_page_num = 1
            var _orgParams = {
                q_word: q_word,
                pageNumber: which_page_num,
                pageSize: p.pageSize,
                andOr: p.andOr,
                searchTable: p.dbTable
            }
            if (p.orderBy !== false) _orgParams.orderBy = p.orderBy
            _orgParams[searchKey] = q_word[0]
            if (_paramsFunc && $.isFunction(_paramsFunc)) {
                var result = _paramsFunc.call(self)
                if (result && $.isPlainObject(result)) {
                    _params = $.extend({}, _orgParams, result)
                } else _params = _orgParams
            } else _params = _orgParams
            self.prop.xhr = $.ajax({
                dataType: 'json',
                url: p.data,
                type: 'POST',
                data: _params,
                success: function (returnData) {
                    if (!returnData || !$.isPlainObject(returnData)) {
                        self.hideResults(self)
                        self.ajaxErrorNotify(self)
                        return
                    }
                    var data = {},
                        json = {}
                    try {
                        data = p.eAjaxSuccess(returnData)
                        json.originalResult = data.list
                        json.cnt_whole = data.totalRow
                    } catch (e) {
                        self.showMessage(self, self.message.ajax_error)
                        return
                    }

                    json.candidate = []
                    json.keyField = []
                    if (typeof json.originalResult != 'object') {
                        self.prop.xhr = null
                        self.notFoundSearch(self)
                        return
                    }
                    json.cnt_page = json.originalResult.length
                    for (var i = 0; i < json.cnt_page; i++) {
                        for (var key in json.originalResult[i]) {
                            if (key == p.keyField) {
                                json.keyField.push(json.originalResult[i][key])
                            }
                            if (key == p.showField) {
                                json.candidate.push(json.originalResult[i][key])
                            }
                        }
                    }
                    self.prepareResults(self, json, q_word, which_page_num)
                },
                error: function (jqXHR, textStatus) {
                    if (textStatus != 'abort') {
                        self.hideResults(self)
                        self.ajaxErrorNotify(self)
                    }
                },
                complete: function () {
                    self.prop.xhr = null
                }
            })
        }

        /**
         * Search for json data source
         * @param {Object} self
         * @param {Array} q_word
         * @param {number} which_page_num
         */
        SelectPage.prototype.searchForJson = function (self, q_word, which_page_num) {
            var p = self.option,
                matched = [],
                esc_q = [],
                sorted = [],
                json = {},
                i = 0,
                arr_reg = []

            //query keyword filter
            do {
                //'/\W/g'正则代表全部不是字母，数字，下划线，汉字的字符
                //将非法字符进行转义
                esc_q[i] = q_word[i].replace(/\W/g, '\\$&').toString()
                arr_reg[i] = new RegExp(esc_q[i], 'gi')
                i++
            } while (i < q_word.length)

            // SELECT * FROM data WHERE field LIKE q_word;
            for (i = 0; i < p.data.length; i++) {
                var flag = false,
                    row = p.data[i],
                    itemText
                for (var j = 0; j < arr_reg.length; j++) {
                    itemText = row[p.searchField]
                    if (p.formatItem && $.isFunction(p.formatItem))
                        itemText = p.formatItem(row)
                    if (itemText.match(arr_reg[j])) {
                        flag = true
                        if (p.andOr == 'OR') break
                    } else {
                        flag = false
                        if (p.andOr == 'AND') break
                    }
                }
                if (flag) matched.push(row)
            }

            // (CASE WHEN ...) then く order some column
            if (p.orderBy === false) sorted = matched.concat()
            else {
                var reg1 = new RegExp('^' + esc_q[0] + '$', 'gi'),
                    reg2 = new RegExp('^' + esc_q[0], 'gi'),
                    matched1 = [],
                    matched2 = [],
                    matched3 = []
                for (i = 0; i < matched.length; i++) {
                    var orderField = p.orderBy[0][0]
                    var orderValue = String(matched[i][orderField])
                    if (orderValue.match(reg1)) {
                        matched1.push(matched[i])
                    } else if (orderValue.match(reg2)) {
                        matched2.push(matched[i])
                    } else {
                        matched3.push(matched[i])
                    }
                }

                if (p.orderBy[0][1].match(/^asc$/i)) {
                    matched1 = self.sortAsc(self, matched1)
                    matched2 = self.sortAsc(self, matched2)
                    matched3 = self.sortAsc(self, matched3)
                } else {
                    matched1 = self.sortDesc(self, matched1)
                    matched2 = self.sortDesc(self, matched2)
                    matched3 = self.sortDesc(self, matched3)
                }
                sorted = sorted.concat(matched1).concat(matched2).concat(matched3)
            }

            /*
                if (sorted.length === undefined || sorted.length === 0 ) {
                    self.notFoundSearch(self);
                    return;
                }
                */
            json.cnt_whole = sorted.length
            //page_move used to distinguish between init plugin or page moving
            if (!self.prop.page_move) {
                //only single mode can be used page number relocation
                if (!p.multiple) {
                    //get selected item belong page number
                    var currentValue = self.elem.hidden.val()
                    if (
                        $.type(currentValue) !== 'undefined' &&
                        $.trim(currentValue) !== ''
                    ) {
                        var index = 0
                        $.each(sorted, function (i, row) {
                            if (row[p.keyField] == currentValue) {
                                index = i + 1
                                return false
                            }
                        })
                        which_page_num = Math.ceil(index / p.pageSize)
                        if (which_page_num < 1) which_page_num = 1
                        self.prop.current_page = which_page_num
                    }
                }
            } else {
                //set page number to 1 when result number less then page size
                if (sorted.length <= (which_page_num - 1) * p.pageSize) {
                    which_page_num = 1
                    self.prop.current_page = 1
                }
            }

            //LIMIT xx OFFSET xx
            var start = (which_page_num - 1) * p.pageSize,
                end = start + p.pageSize
            //save original data
            json.originalResult = []
            //after data filter handle
            for (i = start; i < end; i++) {
                if (sorted[i] === undefined) break
                json.originalResult.push(sorted[i])
                for (var key in sorted[i]) {
                    if (key == p.keyField) {
                        if (json.keyField === undefined) json.keyField = []
                        json.keyField.push(sorted[i][key])
                    }
                    if (key == p.showField) {
                        if (json.candidate === undefined) json.candidate = []
                        json.candidate.push(sorted[i][key])
                    }
                }
            }

            if (json.candidate === undefined) json.candidate = []
            json.cnt_page = json.candidate.length
            self.prepareResults(self, json, q_word, which_page_num)
        }

        /**
         * Set order asc
         * @param {Object} self
         * @param {Array} arr - result array
         */
        SelectPage.prototype.sortAsc = function (self, arr) {
            arr.sort(function (a, b) {
                var valA = a[self.option.orderBy[0][0]],
                    valB = b[self.option.orderBy[0][0]]
                return $.type(valA) === 'number'
                    ? valA - valB
                    : String(valA).localeCompare(String(valB))
            })
            return arr
        }

        /**
         * Set order desc
         * @param {Object} self
         * @param {Array} arr - result array
         */
        SelectPage.prototype.sortDesc = function (self, arr) {
            arr.sort(function (a, b) {
                var valA = a[self.option.orderBy[0][0]],
                    valB = b[self.option.orderBy[0][0]]
                return $.type(valA) === 'number'
                    ? valB - valA
                    : String(valB).localeCompare(String(valA))
            })
            return arr
        }

        /**
         * Not result found handle
         * @param {Object} self
         */
        SelectPage.prototype.notFoundSearch = function (self) {
            self.elem.results.empty()
            self.calcResultsSize(self)
            self.setOpenStatus(self, true)
            self.setCssFocusedInput(self)
        }

        /**
         * Prepare data to show
         * @param {Object} self
         * @param {Object} json - data result
         * @param {Array} q_word - query keyword
         * @param {number} which_page_num - target page number
         */
        SelectPage.prototype.prepareResults = function (
            self,
            json,
            q_word,
            which_page_num
        ) {
            if (self.option.pagination)
                self.setNavi(self, json.cnt_whole, json.cnt_page, which_page_num)

            if (!json.keyField) json.keyField = false

            if (
                self.option.selectOnly &&
                json.candidate.length === 1 &&
                json.candidate[0] == q_word[0]
            ) {
                self.elem.hidden.val(json.keyField[0])
                this.setButtonAttrDefault()
            }
            var is_query = false
            if (q_word && q_word.length && q_word[0]) is_query = true
            self.displayResults(self, json, is_query)
        }

        /**
         * Build page bar
         * @param {Object} self
         * @param {number} cnt_whole - total record count
         * @param {number} cnt_page
         * @param {number} page_num - current page number
         */
        SelectPage.prototype.setNavi = function (
            self,
            cnt_whole,
            cnt_page,
            page_num
        ) {
            var msg = self.message
            /**
             * build pagination bar
             */
            var buildPageNav = function (self, pagebar, page_num, last_page) {
                var updatePageInfo = function () {
                    var pageInfo = msg.page_info
                    return pageInfo
                        .replace(self.template.page.current, page_num)
                        .replace(self.template.page.total, last_page)
                }
                if (pagebar.find('li').length === 0) {
                    pagebar.hide().empty()
                    var iconFist = 'sp-iconfont if-first',
                        iconPrev = 'sp-iconfont if-previous',
                        iconNext = 'sp-iconfont if-next',
                        iconLast = 'sp-iconfont if-last'

                    pagebar.append(
                        '<li class="csFirstPage" title="' +
                        msg.first_title +
                        '" ><a href="javascript:void(0);"> <i class="' +
                        iconFist +
                        '"></i> </a></li>'
                    )
                    pagebar.append(
                        '<li class="csPreviousPage" title="' +
                        msg.prev_title +
                        '" ><a href="javascript:void(0);"><i class="' +
                        iconPrev +
                        '"></i></a></li>'
                    )
                    //pagination information
                    pagebar.append(
                        '<li class="pageInfoBox"><a href="javascript:void(0);"> ' +
                        updatePageInfo() +
                        ' </a></li>'
                    )

                    pagebar.append(
                        '<li class="csNextPage" title="' +
                        msg.next_title +
                        '" ><a href="javascript:void(0);"><i class="' +
                        iconNext +
                        '"></i></a></li>'
                    )
                    pagebar.append(
                        '<li class="csLastPage" title="' +
                        msg.last_title +
                        '" ><a href="javascript:void(0);"> <i class="' +
                        iconLast +
                        '"></i> </a></li>'
                    )
                    pagebar.show()
                } else {
                    pagebar.find('li.pageInfoBox a').html(updatePageInfo())
                }
            }

            var pagebar = self.elem.navi.find('ul'),
                last_page = Math.ceil(cnt_whole / self.option.pageSize) //calculate total page
            if (last_page === 0) page_num = 0
            else {
                if (last_page < page_num) page_num = last_page
                else if (page_num === 0) page_num = 1
            }
            self.prop.current_page = page_num //update current page number
            self.prop.max_page = last_page //update page count
            buildPageNav(self, pagebar, page_num, last_page)

            //update paging status
            var dClass = 'disabled',
                first = pagebar.find('li.csFirstPage'),
                previous = pagebar.find('li.csPreviousPage'),
                next = pagebar.find('li.csNextPage'),
                last = pagebar.find('li.csLastPage')
            //first and previous
            if (page_num === 1 || page_num === 0) {
                if (!first.hasClass(dClass)) first.addClass(dClass)
                if (!previous.hasClass(dClass)) previous.addClass(dClass)
            } else {
                if (first.hasClass(dClass)) first.removeClass(dClass)
                if (previous.hasClass(dClass)) previous.removeClass(dClass)
            }
            //next and last
            if (page_num === last_page || last_page === 0) {
                if (!next.hasClass(dClass)) next.addClass(dClass)
                if (!last.hasClass(dClass)) last.addClass(dClass)
            } else {
                if (next.hasClass(dClass)) next.removeClass(dClass)
                if (last.hasClass(dClass)) last.removeClass(dClass)
            }

            if (last_page > 1) self.ePaging() //pagination event bind
        }

        /**
         * Render result list
         * @param {Object} self
         * @param {Object} json - result data
         * @param {boolean} is_query - used to different from search to open and just click to open
         */
        SelectPage.prototype.displayResults = function (self, json, is_query) {
            var p = self.option, el = self.elem

            el.results.hide().empty()

            if (
                p.multiple &&
                $.type(p.maxSelectLimit) === 'number' &&
                p.maxSelectLimit > 0
            ) {
                var selectedSize = el.element_box.find('li.selected_tag').length
                if (selectedSize > 0 && selectedSize >= p.maxSelectLimit) {
                    var msg = self.message.max_selected
                    self.showMessage(
                        self,
                        msg.replace(self.template.msg.maxSelectLimit, p.maxSelectLimit)
                    )
                    return
                }
            }

            if (json.candidate.length) {
                var arr_candidate = json.candidate,
                    arr_primary_key = json.keyField,
                    keystr = el.hidden.val(),
                    keyArr = keystr ? keystr.split(',') : new Array(),
                    itemText = ''
                for (var i = 0; i < arr_candidate.length; i++) {
                    if (p.formatItem && $.isFunction(p.formatItem)) {
                        try {
                            itemText = p.formatItem(json.originalResult[i])
                        } catch (e) {
                            /*eslint no-console: ["error", { allow: ["warn", "error"] }] */
                            console.error('formatItem 内容格式化函数内容设置不正确！')
                            itemText = arr_candidate[i]
                        }
                    } else {
                        itemText = arr_candidate[i]
                    }

                    var list = $('<li>').html(itemText).attr({
                        pkey: arr_primary_key[i]
                    })

                    if (!p.formatItem) list.attr('title', itemText)

                    //Set selected item highlight
                    if ($.inArray(arr_primary_key[i].toString(), keyArr) !== -1) {
                        list.addClass(self.css_class.selected)
                    }
                    //cache item data
                    list.data('dataObj', json.originalResult[i])
                    el.results.append(list)
                }
            } else {
                var li =
                    '<li class="' +
                    self.css_class.message_box +
                    '"><i class="sp-iconfont if-warning"></i> ' +
                    self.message.not_found +
                    '</li>'
                el.results.append(li)
            }
            el.results.show()

            if (p.multiple && p.multipleControlbar) el.control.show()
            if (p.pagination) el.navi.show()
            self.calcResultsSize(self)
            self.setOpenStatus(self, true)

            //Result item event bind
            self.eResultList()
            //scrolling listen
            self.eScroll()
            //auto highlight first item in search, have result and set autoSelectFirst to true situation
            if (is_query && json.candidate.length && p.autoSelectFirst)
                self.nextLine(self)
        }

        /**
         * Calculate result list size and position
         * @param {Object} self
         */
        SelectPage.prototype.calcResultsSize = function (self) {
            var p = self.option,
                el = self.elem
            var rePosition = function () {
                if (el.container.css('position') === 'static') {
                    // position: static
                    var st_offset = el.combo_input.offset()
                    el.result_area.css({
                        top: st_offset.top + el.combo_input.outerHeight() + 'px',
                        left: st_offset.left + 'px'
                    })
                } else {
                    var listHeight
                    if (!p.pagination) {
                        var itemHeight = el.results.find('li:first').outerHeight(true)
                        listHeight = itemHeight * p.listSize
                        el.results.css({
                            'max-height': listHeight,
                            'overflow-y': 'auto'
                        })
                    }

                    //handle result list show up side(left, right, up or down)
                    var docWidth = $(document).width(),
                        docHeight = $(document).height(), //the document full height
                        viewHeight = $(window).height(), //browser visible area height
                        offset = el.container.offset(),
                        screenScrollTop = $(window).scrollTop(),
                        listWidth = el.result_area.outerWidth(),
                        //default left used input element left
                        defaultLeft = offset.left, //p.multiple ? -1 : 0;
                        //input element height
                        inputHeight = el.container.outerHeight(),
                        left =
                            offset.left + listWidth > docWidth
                                ? defaultLeft - (listWidth - el.container.outerWidth())
                                : defaultLeft,
                        //the actual top coordinate of input element(outer div)
                        screenTop = offset.top, //$(el.container).scrollTop();//offset.top - screenScrollTop;
                        top = 0,
                        dist = 5, //set distance between input element and result list
                        //the actual top coordinate of result list
                        listBottom = screenTop + inputHeight + listHeight + dist,
                        hasOverflow = docHeight > viewHeight

                    //result list height
                    listHeight = el.result_area.outerHeight()

                    if (
                        (screenTop - screenScrollTop - dist > listHeight &&
                            hasOverflow &&
                            listBottom > viewHeight + screenScrollTop) ||
                        (!hasOverflow && listBottom > viewHeight)
                    ) {
                        //open up
                        top = offset.top - listHeight - dist
                        el.result_area.removeClass('shadowUp shadowDown').addClass('shadowUp')
                    } else {
                        //open down
                        top =
                            offset.top + (p.multiple ? el.container.outerHeight() : inputHeight)
                        el.result_area
                            .removeClass('shadowUp shadowDown')
                            .addClass('shadowDown')
                        top += dist
                    }
                    return {
                        top: top + 'px',
                        left: left + 'px'
                    }
                }
            }
            if (el.result_area.is(':visible')) {
                el.result_area.css(rePosition())
            } else {
                var pss = rePosition()
                el.result_area.css(pss).show(1, function () {
                    var repss = rePosition()
                    if (pss.top !== repss.top || pss.left !== repss.left)
                        el.result_area.css(repss)
                })
            }
        }

        /**
         * hide result list
         * @param {Object} self
         */
        SelectPage.prototype.hideResults = function (self) {
            if (self.prop.key_paging) {
                self.scrollWindow(self, true)
                self.prop.key_paging = false
            }
            self.setCssFocusedInput(self)

            if (self.option.autoFillResult) {
                //self.selectCurrentLine(self, true);
            }

            self.elem.results.empty()
            self.elem.result_area.hide()
            self.setOpenStatus(self, false)
            //unbind window scroll listen
            $(window).off('scroll.SelectPage')

            self.abortAjax(self)
            self.setButtonAttrDefault()
        }

        /**
         * set plugin to disabled / enabled
         * @param self
         * @param disabled
         */
        SelectPage.prototype.disabled = function (self, disabled) {
            var el = self.elem
            if ($.type(disabled) === 'undefined') return el.combo_input.prop('disabled')
            if ($.type(disabled) === 'boolean') {
                el.combo_input.prop('disabled', disabled)
                if (disabled) el.container.addClass(self.css_class.disabled)
                else el.container.removeClass(self.css_class.disabled)
            }
        }

        /**
         * Go fist page
         * @param {Object} self
         */
        SelectPage.prototype.firstPage = function (self) {
            if (self.prop.current_page > 1) {
                self.prop.current_page = 1
                self.prop.page_move = true
                self.suggest(self)
            }
        }

        /**
         * Go previous page
         * @param {Object} self
         */
        SelectPage.prototype.prevPage = function (self) {
            if (self.prop.current_page > 1) {
                self.prop.current_page--
                self.prop.page_move = true
                self.suggest(self)
            }
        }

        /**
         * Go next page
         * @param {Object} self
         */
        SelectPage.prototype.nextPage = function (self) {
            if (self.prop.current_page < self.prop.max_page) {
                self.prop.current_page++
                self.prop.page_move = true
                self.suggest(self)
            }
        }

        /**
         * Go last page
         * @param {Object} self
         */
        SelectPage.prototype.lastPage = function (self) {
            if (self.prop.current_page < self.prop.max_page) {
                self.prop.current_page = self.prop.max_page
                self.prop.page_move = true
                self.suggest(self)
            }
        }
        /**
         * do something after select/unSelect action
         * @param {Object} self
         * @param {boolean} reOpen
         */
        SelectPage.prototype.afterAction = function (self, reOpen) {
            self.inputResize(self)
            self.elem.combo_input.change()
            self.setCssFocusedInput(self)
            if (self.prop.init_set) return
            if (self.option.multiple) {
                if (self.option.selectToCloseList) {
                    self.hideResults(self)
                    self.elem.combo_input.blur()
                }
                if (!self.option.selectToCloseList && reOpen) {
                    self.suggest(self)
                    self.elem.combo_input.focus()
                }
            } else {
                self.hideResults(self)
                self.elem.combo_input.blur()
            }
        }

        /**
         * Select current list item
         * @param {Object} self
         */
        SelectPage.prototype.selectCurrentLine = function (self) {
            self.scrollWindow(self, true)

            var p = self.option, current = self.getCurrentLine(self)
            if (current) {
                var data = current.data('dataObj')
                if (p.multiple) {
                    // build tags in multiple selection mode
                    self.elem.combo_input.val('')
                    var item = {text: current.text(), value: current.attr('pkey')}
                    if (!self.isAlreadySelected(self, item)) {
                        self.addNewTag(self, data, item)
                        self.tagValuesSet(self)
                    }
                } else {
                    self.elem.combo_input.val(current.text())
                    self.elem.combo_input.data('dataObj', data)
                    self.elem.hidden.val(current.attr('pkey'))
                }

                if (p.selectOnly) self.setButtonAttrDefault()

                // Select item callback
                if (p.eSelect && $.isFunction(p.eSelect)) p.eSelect(data, self)

                self.prop.prev_value = self.elem.combo_input.val()
                self.prop.selected_text = self.elem.combo_input.val()

                self.putClearButton()
            }
            self.afterAction(self, true)
        }
        /**
         * Show clear button when item selected in single selection mode
         */
        SelectPage.prototype.putClearButton = function () {
            if (!this.option.multiple && !this.elem.combo_input.prop('disabled')) {
                this.elem.container.append(this.elem.clear_btn)
            }
        }
        /**
         * Select all list item
         * @param {Object} self
         */
        SelectPage.prototype.selectAllLine = function (self) {
            var p = self.option, jsonarr = new Array()
            self.elem.results.find('li').each(function (i, row) {
                var $row = $(row), data = $row.data('dataObj')
                var item = {text: $row.text(), value: $row.attr('pkey')}
                if (!self.isAlreadySelected(self, item)) {
                    self.addNewTag(self, data, item)
                    self.tagValuesSet(self)
                }
                jsonarr.push(data)
                // limited max selected items
                if (
                    $.type(p.maxSelectLimit) === 'number' &&
                    p.maxSelectLimit > 0 &&
                    p.maxSelectLimit === self.elem.element_box.find('li.selected_tag').length
                ) {
                    return false
                }
            })
            if (p.eSelect && $.isFunction(p.eSelect)) p.eSelect(jsonarr, self)
            self.afterAction(self, true)
        }
        /**
         * Cancel select all item in current page
         * @param {Object} self
         */
        SelectPage.prototype.unSelectAllLine = function (self) {
            var p = self.option,
                ds = []
            self.elem.results.find('li').each(function (i, row) {
                var key = $(row).attr('pkey')
                var tag = self.elem.element_box.find(
                    'li.selected_tag[itemvalue="' + key + '"]'
                )
                if (tag.length) ds.push(tag.data('dataObj'))
                self.removeTag(self, tag)
            })
            self.afterAction(self, true)
            if (p.eTagRemove && $.isFunction(p.eTagRemove)) p.eTagRemove(ds)
        }
        /**
         * Clear all selected items
         * @param {Object} self
         * @param {boolean} open - open list after clear selected item
         */
        SelectPage.prototype.clearAll = function (self, open) {
            var p = self.option, ds = []
            if (p.multiple) {
                self.elem.element_box.find('li.selected_tag').each(function (i, row) {
                    ds.push($(row).data('dataObj'))
                    row.remove()
                })
                self.elem.element_box.find('li.selected_tag').remove()
            } else {
                $(self.elem.combo_input).removeData('dataObj')
            }
            self.reset(self)
            self.afterAction(self, open)

            if (p.multiple) {
                if (p.eTagRemove && $.isFunction(p.eTagRemove)) p.eTagRemove(ds)
            } else self.elem.clear_btn.remove()
        }

        /**
         * reset
         */
        SelectPage.prototype.reset = function (self) {
            self.elem.combo_input.val('')
            self.elem.hidden.val('')
            self.prop.prev_value = ''
            self.prop.selected_text = ''
            self.prop.current_page = 1
        }

        /**
         * Get current highlight item
         * @param {Object} self
         */
        SelectPage.prototype.getCurrentLine = function (self) {
            if (self.elem.result_area.is(':hidden')) return false
            var obj = self.elem.results.find('li.' + self.css_class.select)
            return obj.length ? obj : false
        }

        /**
         * Check the result item is already selected or not
         * @param {Object} self
         * @param {Object} item - item info
         */
        SelectPage.prototype.isAlreadySelected = function (self, item) {
            var isExist = false
            if (item.value) {
                var keys = self.elem.hidden.val()
                if (keys) {
                    var karr = keys.split(',')
                    if (karr && karr.length && $.inArray(item.value, karr) != -1)
                        isExist = true
                }
            }
            return isExist
        }

        /**
         * Add a new tag in multiple selection mode
         * @param {Object} self
         * @param {object} data - raw row data
         * @param {Object} item
         */
        SelectPage.prototype.addNewTag = function (self, data, item) {
            if (!self.option.multiple || !data || !item) return
            var tmp = self.template.tag.content, tag
            tmp = tmp.replace(self.template.tag.textKey, item.text)
            tmp = tmp.replace(self.template.tag.valueKey, item.value)
            tag = $(tmp)
            tag.data('dataObj', data)
            if (self.elem.combo_input.prop('disabled')) {
                tag.find('span.tag_close').hide()
            }

            self.elem.combo_input.closest('li').before(tag)
        }

        /**
         * Remove a tag in multiple selection mode
         * @param {Object} self
         * @param {Object} item
         */
        SelectPage.prototype.removeTag = function (self, item) {
            var key = $(item).attr('itemvalue')
            var keys = self.elem.hidden.val()
            if ($.type(key) != 'undefined' && keys) {
                var keyarr = keys.split(','),
                    index = $.inArray(key.toString(), keyarr)
                if (index != -1) {
                    keyarr.splice(index, 1)
                    self.elem.hidden.val(keyarr.toString())
                }
            }
            $(item).remove()
            self.inputResize(self)
        }

        /**
         * Selected item value(keyField) put in to hidden element
         * @param {Object} self
         */
        SelectPage.prototype.tagValuesSet = function (self) {
            if (!self.option.multiple) return
            var tags = self.elem.element_box.find('li.selected_tag')
            if (tags && tags.length) {
                var result = new Array()
                $.each(tags, function (i, li) {
                    var v = $(li).attr('itemvalue')
                    if ($.type(v) !== 'undefined') result.push(v)
                })
                if (result.length) {
                    self.elem.hidden.val(result.join(','))
                }
            }
        }

        /**
         * auto resize input element width in multiple select mode
         * @param {Object} self
         */
        SelectPage.prototype.inputResize = function (self) {
            if (!self.option.multiple) return
            var inputLi = self.elem.combo_input.closest('li')
            var setDefaultSize = function (self, inputLi) {
                inputLi.removeClass('full_width')
                var minimumWidth = self.elem.combo_input.val().length + 1,
                    width = minimumWidth * 0.75 + 'em'
                self.elem.combo_input.css('width', width).removeAttr('placeholder')
            }
            if (self.elem.element_box.find('li.selected_tag').length === 0) {
                if (self.elem.combo_input.attr('placeholder_bak')) {
                    if (!inputLi.hasClass('full_width')) inputLi.addClass('full_width')
                    self.elem.combo_input
                        .attr('placeholder', self.elem.combo_input.attr('placeholder_bak'))
                        .removeAttr('style')
                } else setDefaultSize(self, inputLi)
            } else setDefaultSize(self, inputLi)
        }

        /**
         * Move to next line
         * @param {Object} self
         */
        SelectPage.prototype.nextLine = function (self) {
            var obj = self.getCurrentLine(self), idx
            if (!obj) {
                idx = -1
            } else {
                idx = self.elem.results.children('li').index(obj)
                obj.removeClass(self.css_class.select)
            }

            idx++

            if (idx < self.elem.results.children('li').length) {
                var next = self.elem.results.children('li').eq(idx)
                next.addClass(self.css_class.select)
                self.setCssFocusedResults(self)
            } else {
                self.setCssFocusedInput(self)
            }
            self.scrollWindow(self, false)
        }

        /**
         * Move to previous line
         * @param {Object} self
         */
        SelectPage.prototype.prevLine = function (self) {
            var obj = self.getCurrentLine(self), idx
            if (!obj) idx = self.elem.results.children('li').length
            else {
                idx = self.elem.results.children('li').index(obj)
                obj.removeClass(self.css_class.select)
            }
            idx--
            if (idx > -1) {
                var prev = self.elem.results.children('li').eq(idx)
                prev.addClass(self.css_class.select)
                self.setCssFocusedResults(self)
            } else self.setCssFocusedInput(self)
            self.scrollWindow(self, false)
        }

        /**
         * SelectPage plugin definition
         * @global
         * @param option {Object} init plugin option
         */
        function Plugin(option) {
            return this.each(function () {
                var $this = $(this),
                    data = $this.data(SelectPage.dataKey),
                    params = $.extend(
                        {},
                        defaults,
                        $this.data(),
                        data && data.option,
                        typeof option === 'object' && option
                    )
                if (!data)
                    $this.data(SelectPage.dataKey, (data = new SelectPage(this, params)))
            })
        }

        /**
         * Get plugin object
         * @param {object} obj
         * @returns
         */
        function getPlugin(obj) {
            return $(obj).closest('div.sp_container').find('input.sp_input')
        }

        /**
         * Clear all selected item
         */
        function ClearSelected() {
            return this.each(function () {
                var $this = getPlugin(this),
                    data = $this.data(SelectPage.dataKey)
                if (data) {
                    data.prop.init_set = true
                    data.clearAll(data)
                    data.prop.init_set = false
                }
            })
        }

        /**
         * Refresh result list
         *
         * use case:
         * 1.use $(obj).val('xxx') to modify selectpage selected item key
         * 2.refresh selected item show content/tag text
         */
        function SelectedRefresh() {
            return this.each(function () {
                var $this = getPlugin(this),
                    data = $this.data(SelectPage.dataKey)
                if (data && data.elem.hidden.val()) data.setInitRecord(true)
            })
        }

        /**
         * Modify plugin datasource, only work on json datasource mode
         * @param {array} data - new datasource
         * @example
         * [{name:'aa',sex:1},{name:'bb',sex:0},{...}]
         */
        function ModifyDataSource(data) {
            return this.each(function () {
                if (data && $.isArray(data)) {
                    var $this = getPlugin(this),
                        plugin = $this.data(SelectPage.dataKey)
                    if (plugin) {
                        plugin.clearAll(plugin)
                        plugin.option.data = data
                    }
                }
            })
        }

        /**
         * Get plugin disabled status or Modify plugin disabled status
         * @param {boolean} disabled - set disabled status
         */
        function PluginDisabled(disabled) {
            var status = false
            this.each(function () {
                var $this = getPlugin(this),
                    plugin = $this.data(SelectPage.dataKey)
                if (plugin) {
                    if ($.type(disabled) !== 'undefined') plugin.disabled(plugin, disabled)
                    else status = plugin.disabled(plugin)
                }
            })
            return status
        }

        /**
         * Get selected item text
         * @returns {string}
         */
        function GetInputText() {
            var str = ''
            this.each(function () {
                var $this = getPlugin(this), data = $this.data(SelectPage.dataKey)
                if (data) {
                    if (data.option.multiple) {
                        var tags = []
                        data.elem.element_box.find('li.selected_tag').each(function (i, tag) {
                            tags.push($(tag).text())
                        })
                        str += tags.toString()
                    } else str += data.elem.combo_input.val()
                }
            })
            return str
        }

        /**
         * Get selected item raw data
         * @returns {any[]}
         */
        function GetSelectedData() {
            var results = []
            this.each(function () {
                var $this = getPlugin(this), data = $this.data(SelectPage.dataKey)
                if (data) {
                    if (data.option.multiple) {
                        data.elem.element_box.find('li.selected_tag').each(function (i, tag) {
                            results.push($(tag).data('dataObj'))
                        })
                    } else {
                        var selected = data.elem.combo_input.data('dataObj')
                        if (selected) results.push(selected)
                    }
                }
            })

            return results
        }

        var old = $.fn.selectPage;

        $.fn.selectPage = Plugin
        $.fn.selectPage.Constructor = SelectPage
        $.fn.selectPageClear = ClearSelected
        $.fn.selectPageRefresh = SelectedRefresh
        $.fn.selectPageData = ModifyDataSource
        $.fn.selectPageDisabled = PluginDisabled
        $.fn.selectPageText = GetInputText
        $.fn.selectPageSelectedData = GetSelectedData

        // SelectPage no conflict
        // =================
        $.fn.selectPage.noConflict = function () {
            $.fn.selectPage = old
            return this
        }
    })(window.jQuery)
</script>