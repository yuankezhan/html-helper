<?php
?>
<style>
    :root
    {
        --green : #009688;
        --orange : #FF5722;
        --blue : #1E9FFF;
        --yellow : #FFB800;
        --black : #2F4056;
        --white : #fff;
        --light-gray : #ccc;
        --dark : #666;

        --padding: 4px;
        --width: 240px;
        --height: 38px;
        --border: 1px solid var(--light-gray);
        --border-radius : 4px;
    }
    .helper-create-select, .helper-create-input
    {
        padding: var(--padding);
        width: var(--width);
        height: var(--height);
        color: var(--dark);
        border-radius: var(--border-radius);
        border: var(--border);
    }
    .helper-create-button
    {
        padding: 2px 10px;
        height: var(--height);
        border-radius: var(--border-radius);
        border: unset;
        cursor: pointer;
        outline: unset;
        background: var(--green);
        color: var(--white);
        min-width: 60px;
    }
    .helper-create-button:hover {
        opacity: .9;
        filter: alpha(opacity=90);
        color: #fff;
    }
    .helper-create-button:active {
        opacity: 1;
        filter: alpha(opacity=100);
    }
    .helper-create-button:disabled {
        cursor: not-allowed;
        opacity: 1;
    }
</style>
